from django.apps import AppConfig


class ArkhamConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'arkham'

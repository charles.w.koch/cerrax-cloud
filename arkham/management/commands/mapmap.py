
import os
import glob

from django.conf import settings
from django.core.management.base import BaseCommand

from arkham.tools.mapread import MapReader, MapWriter

#-== @h1
# Arkham Management Commands
#-== /arkham.management.commands
#________________________________________

#-== @method
# manage mapmap [file or directory]

#-== This command converts /.amap files to /.json .
# This allows developers to use the human-friendly /.amap format
# and then convert it to a more machine-readable format for the disply routine.
# The converted /.json file is sent to the Arkham app static files directory ( /-arkham/static/arkham/mapdata-/ ).

#-== If no file or directory is provided to the command,
# it will search /-arkham/mapfiles-/ directory and convert each /.amap file it finds.

#-== If a filename with the /.amap extension is given, it will convert only that file.

#-== If a directory is given, it will convert all /.amap files in that directory.
# @note
# The directory search is not recursive. Only first level files will be found.

class Command(BaseCommand):
    help = 'Generates a JSON file which Arkham can read to display the map'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('-m', '--mapfile', dest="mapfile", default=None, required=False)

    def handle(self, *args, **options):
        mapfile = options['mapfile']
        if mapfile is None:
            mapfile = os.path.join(settings.BASE_DIR, 'arkham', 'mapfiles')
        self.convert(mapfile)

    def convert(self, mapfile):
        if os.path.isdir(mapfile):
            filenames = next(os.walk(mapfile), (None, None, []))[2]
            for filename in filenames:
                if filename[-5:] == '.amap':
                    fullpath = os.path.join(mapfile, filename)
                    self.convert_mapfile(fullpath, filename)
        else:
            self.convert_mapfile(mapfile, mapfile)

    def convert_mapfile(self, mapfile, filename):
        mapfile_name = filename[:-5]
        abs_path = os.path.abspath(mapfile)

        reader = MapReader()
        data = reader.read_file(abs_path)
        writer = MapWriter()
        jsondata = writer.write_data(data)

        outfile_path = mapfile_name + '.json'
        outfile_path = os.path.join(settings.BASE_DIR,
                                'arkham', 'static', 'arkham', 'mapdata', outfile_path)
        #print('writing JSON to {}'.format(outfile_path))
        with open(outfile_path, 'w') as outfile:
            outfile.write(jsondata)
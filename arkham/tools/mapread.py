import sys
import json
from pprint import pp as pretty
import xml.etree.ElementTree as ETreeModule
from xml.etree.ElementTree import ElementTree as ETree
from xml.etree.ElementTree import Element as EL
import xml.dom.minidom as md

import cerrax
from cerrax import DotDict as dd


##########################################
#-== @class
class MapReader:
	#-== Consumes /.amap files and converts them into dictionaries for use in Python.

	SECTION_MARKER = '_____'

	DOOR_DIRS = dd(
		NW = dd( x = -1, y = 1, valid_doors = '\\X*' ),
		N =  dd( x = 0, y = 1, valid_doors = '|^+*'),
		NE = dd( x = 1, y = 1, valid_doors = '/X*' ),
		E =  dd( x = 1, y = 0, valid_doors = '-+>*' ),
		SE = dd( x = 1, y = -1, valid_doors = '\\X*' ),
		S =  dd( x = 0, y = -1, valid_doors = '|v+*' ),
		SW = dd( x = -1, y = -1, valid_doors = '/X*' ),
		W =  dd( x = -1, y = 0, valid_doors = '-+<*' ),
	)


	def __init__(self):
		self.initialize()

	def initialize(self):
		self.curr_section = None
		self.curr_obj = None
		self.map = None
		self.locations = dd()
		self.doors = dd()

	# ----------------------------------------------------
	#-== @method
	def read_file(self, filepath):
		#-== Takes an /.amap file and converts it to a Python dictonary.
		# @params
		# filepath: a valid /.amap file to convert
		# @return
		# A Python dictionary of the /.amap file's contents

		#-== This method injects some additional data
		# into the resulting dictonary, such as position and direction,
		# which are parsed from the MAP section of the file.

		self.initialize()
		with open(filepath, 'r') as infile:
			for line in infile.readlines():
				self.read_line(line)

		self.cleanup_doors()

		#pretty(self.locations, indent=2)
		#pretty(self.doors, indent=2)

		return dd(
			locations = self.locations,
			doors = self.doors
		)

	def read_line(self, line):
		stripped_line = line.strip()
		if stripped_line.startswith(self.SECTION_MARKER):
			self.end_section()
			section = stripped_line.strip('_')
			self.curr_section = section
			#print('Starting {} section'.format(section))
		elif self.curr_section == 'MAP':
			self.read_line_for_map(line)
		elif self.curr_section == 'LOCATIONS':
			self.read_line_for_locations(line)
		elif self.curr_section == 'DOORS':
			self.read_line_for_doors(line)


	def read_line_for_map(self, line):
		if not self.curr_obj:
			self.curr_obj = dd( height = 0, width = 0, matrix = [] )

		if line.strip() != '':
			self.curr_obj.height += 1
			if len(line) > self.curr_obj.width:
				self.curr_obj.width = len(line)
			self.curr_obj.matrix.append(line)

	def analyze_map_obj(self, obj):
		self.map = cerrax.Grid(
			obj.width,
			obj.height,
			default = ' ',
			data = obj.matrix
		)
		# get locations
		self.map.focus(0, 0)
		for x, y, cell in self.map.typewriter_traverse():
			if cell.isalpha() and cell not in 'Xv':
				self.locations[cell] = dd( name = None, x = x, y = y )
		# follow doors from each location
		for loc_id, data in self.locations.items():
			self.analyze_doors(loc_id, data)


	def analyze_doors(self, loc_id, data):
		for dir in self.DOOR_DIRS.values():
			start = self.map.focus(data.x, data.y)
			self.analyze_direction(loc_id, dir)


	def analyze_direction(self, loc_id, direction, door = None):
		try:
			if direction.x < 0:
				neighbor = self.map.focus_left(-direction.x)
			else:
				neighbor = self.map.focus_right(direction.x)
			if direction.y < 0:
				neighbor = self.map.focus_down(-direction.y)
			else:
				neighbor = self.map.focus_up(direction.y)
		except IndexError:
			# we hit the edge of the map
			return

		if door is not None and neighbor.isalpha() and neighbor not in 'Xv':
			# we found a location!
			newdoor = dd( name = '', twoway = True )
			loc = self.locations[loc_id]
			newdoor['start'] = { 'row': loc.y, 'col': loc.x }
			loc = self.locations[neighbor]
			newdoor['end'] = { 'row': loc.y, 'col': loc.x }
			if door in '<>^v':
				newdoor.twoway = False
			self.doors[loc_id+neighbor] = newdoor

		elif neighbor in direction.valid_doors:
			door = neighbor
			self.analyze_direction(loc_id, direction, door)

	def read_key_val(self, line):
		splitline = line.split(':', 1)
		return splitline[0].strip(), splitline[1].strip()

	def read_line_for_locations(self, line):
		stripped_line = line.strip()
		if stripped_line == '':
			self.curr_obj = None
			return

		if self.curr_obj:
			key, value = self.read_key_val(stripped_line)
			self.curr_obj[key] = value
		else:
			loc_id, name = self.read_key_val(stripped_line)
			self.curr_obj = self.locations[loc_id]
			self.curr_obj.name = name

	def read_line_for_doors(self, line):
		stripped_line = line.strip()
		if stripped_line == '':
			self.curr_obj = None
			return

		if self.curr_obj:
			key, value = self.read_key_val(stripped_line)
			# validate data
			if key == 'twoway':
				value = bool(value.lower() == 'true')
			# store data
			self.curr_obj[key] = value
		else:
			door_id, name = self.read_key_val(stripped_line)
			if door_id not in self.doors.keys():
				self.doors[door_id] = dd( name = '', twoway = True )
			self.curr_obj = self.doors[door_id]
			self.curr_obj.name = name

	def end_section(self):
		if self.curr_section == 'MAP':
			self.analyze_map_obj(self.curr_obj)
		self.curr_obj = None
		self.curr_section = None

	def cleanup_doors(self):
		marked_for_deletion = set()
		for door_id, data in self.doors.items():
			if door_id not in marked_for_deletion:
				if data.twoway:
					reverse_id = door_id[::-1]
					if reverse_id in self.doors.keys() and \
										not self.doors[reverse_id].twoway:
						marked_for_deletion.add(door_id)
					else:
						marked_for_deletion.add(reverse_id)
		for door_id in marked_for_deletion:
			if door_id in self.doors.keys():
				del self.doors[door_id]


##########################################
#-== @class
class MapWriter:
	#-== Consumes the dictionary created by a /MapReader
	# and writes it to different formats.

	TYPES = dd(
		string = 'string',
		int = 'int',
		float = 'double',
		bin = 'boolean',
		xml = 'xml',
	)

	YED_ATTR = {
		'xmlns': 'http://graphml.graphdrawing.org/xmlns',
		'xmlns:java': 'http://www.yworks.com/xml/yfiles-common/1.0/java',
		'xmlns:sys': 'http://www.yworks.com/xml/yfiles-common/markup/primitives/2.0',
		'xmlns:x': 'http://www.yworks.com/xml/yfiles-common/markup/2.0',
		'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
		'xmlns:y': 'http://www.yworks.com/xml/graphml',
		'xmlns:yed': 'http://www.yworks.com/xml/yed/3',
		'xsi:schemaLocation': 'http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd',
	}

	def __init__(self):
		self.root = None
		self.keys = {}
		self.nodes = {}
		self.edges = {}

	def add_key(self, name, keytype = 'string', default = None, fortype = 'node'):
		keynum = len(self.keys.keys())
		keyid = 'k{}'.format(keynum)
		attrs = {
			'id': keyid,
			'attr.name': name,
			'attr.type': keytype,
			'for': fortype,
		}
		keytag = EL('key', attrs)
		if default:
			defaultattrs = { 'xml:space': 'preserve' }
			defaultnode = EL('default', defaultattrs)
			defaultnode.text = default
			keytag.append(defaultnode)
		self.keys[name] = keyid
		self.root.append(keytag)


	def add_location(self, graph, loc_id, location):
		nodenum = len(self.nodes.keys())
		nodeid = 'n{}'.format(nodenum)
		kwargs = { 'id': nodeid }
		newnode = EL('node', kwargs)
		newnode.append(self.datanode('name', location.name))
		newnode.append(self.datanode('row', str(location.y)))
		newnode.append(self.datanode('col', str(location.x)))
		self.nodes[loc_id] = nodeid
		graph.append(newnode)


	def add_door(self, graph, door_id, door):
		edgenum = len(self.edges.keys())
		edgeid = 'e{}'.format(edgenum)
		kwargs = { 
			'id': edgeid,
			'source': self.nodes[door_id[0]],
			'target': self.nodes[door_id[1]],
		}
		newedge = EL('edge', kwargs)
		newedge.append(self.datanode('edgename', door.name))
		newedge.append(self.datanode('twoway', str(door.twoway)))
		self.edges[door_id] = edgeid
		graph.append(newedge)


	def datanode(self, name, value, xml=False):
		attrs = { 'key': self.keys[name] }
		newdata = EL('data', attrs)
		if not xml:
			newdata.text = value
		else:
			newdata.append(value)
		return newdata

	# ----------------------------------------------------
	#-== @method
	def write_editor(self, filename, data):
		#-== Writes a /.graphml file, which can be loaded into a graph program
		# (such as yEd) for further investigation or editing.
		# @params
		# filename: the name of the file to write the GraphML
		# data: the map dictionary from a /MapReader

		doc = ETree(EL('graphml', self.YED_ATTR))
		self.root = doc.getroot()
		#self.add_key('grid', self.TYPES.xml, fortype='graphml')
		self.add_key('name')
		self.add_key('row')
		self.add_key('col')
		self.add_key('edgename', fortype='edge')
		self.add_key('twoway', self.TYPES.bin, default='false', fortype='edge')

		graph = EL('graph', { 'id': 'G1', 'edgedefault': 'directed' })

		for loc_id, location in data.locations.items():
			self.add_location(graph, loc_id, location)

		for door_id, door in data.doors.items():
			self.add_door(graph, door_id, door)

		self.root.append(graph)

		ETreeModule.indent(doc, space="\t", level=0)
		doc.write(filename, encoding='utf-8', xml_declaration=True)

	# ----------------------------------------------------
	#-== @method
	def write_data(self, data):
		#-== Returns a JSON string of the /data dictionary
		# @params
		# data: the map dictionary from a /MapReader

		return json.dumps(data, indent=2)


def main(args):
	
	reader = MapReader()
	data = reader.read_file('map.amap')
	writer = MapWriter()
	#writer.write_editor('map.graphml', data)
	jsonstr = writer.write_data(data)
	# print('____________OTPUT______________')
	# print('')
	# print(jsonstr)

if __name__ == '__main__':
	main(sys.argv[1:])

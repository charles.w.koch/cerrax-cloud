from django.urls import path, include

from arkham import views

urlpatterns = [
    # http://localhost:8000/arkham/test/
    path('test/', views.TestView.as_view(), name = 'test'),
]
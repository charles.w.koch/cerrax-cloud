
var mapData;

var player = {
	location: {
		row: null,
		col: null,
	}
}

function selectorByLabel(tag, label) {
	return `${tag}[inkscape\\:label="${label}"`;
}

function sel(tag, label) {
	return selectorByLabel(tag, label);
}

function getRelativeXY(x, y, element){
	let p = new DOMPoint(x, y);
	let baseTransform = element.transform.baseVal;
	let ctm = baseTransform.consolidate().matrix;
	p = p.matrixTransform(ctm);
	return p
}

function getLocationGraphics(row, col) {
	let data = {
		row: row,
		col: col,
		group: null,
		location: null,
		ports: {},
		arrows: {},
	}
	data.group = document.querySelector(sel("g", `r${row}c${col}`));
	data.location = data.group.querySelector(sel(".location", "location"));
	for(let dir of ["N","NE", "E", "SE", "S", "SW", "W", "NW"]) {
		data.ports[dir] = data.group.querySelector(sel(".port", `port_${dir}`));
		data.arrows[dir] = data.group.querySelector(sel(".arrowhead", `arrow_${dir}`));
	}
	return data;
}

function hide(node) {
	node.style.display = "none";
}

function show(node) {
	node.style.display = "inline";
}

function getDirection(start, end) {
	let direction = "";
	if(start.row < end.row) {
		direction += "S";
	} else if (start.row > end.row) {
		direction += "N"
	}
	if(start.col > end.col) {
		direction += "W";
	} else if (start.col < end.col) {
		direction += "E"
	}
	return direction
}

function drawLine(map, start, end, twoway=true) {
	let dir, x, y, point;
	
	let path = document.createElementNS("http://www.w3.org/2000/svg", "line");
	path.setAttribute("stroke", "black");
	path.setAttribute("stroke-width", 0.2644);
	path.style.display = "inline";

	dir = getDirection(start, end);

	x = parseFloat(start.ports[dir].getAttribute("cx"));
	y = parseFloat(start.ports[dir].getAttribute("cy"));
	point = getRelativeXY(x, y, start.group)
	path.setAttribute("x1", point.x);
	path.setAttribute("y1", point.y);

	if(twoway) {
		show(start.arrows[dir]);
	}

	dir = getDirection(end, start);

	x = parseFloat(end.ports[dir].getAttribute("cx"));
	y = parseFloat(end.ports[dir].getAttribute("cy"));
	point = getRelativeXY(x, y, end.group)
	path.setAttribute("x2", point.x);
	path.setAttribute("y2", point.y);

	show(end.arrows[dir]);

	map.appendChild(path)
}

function drawDoor(map, rowStart, colStart, rowEnd, colEnd, twoway=true) {
	let start = getLocationGraphics(rowStart, colStart);
	show(start.location)

	let end = getLocationGraphics(rowEnd, colEnd);
	show(end.location)

	drawLine(map, start, end, twoway)
}

function drawMap(mapid, mapdata) {
	mapData = mapdata;
	let start_loc;
	let map = document.querySelector(mapid);
	for(const key in mapdata.doors) {
		let door = mapdata.doors[key];
		start_loc = {
			row: door.start.row,
			col: door.start.col,
		}
		drawDoor(map,
			door.start.row, door.start.col,
			door.end.row, door.end.col, 
			door.twoway);
	}
	// set player to start at the last location placed
	moveTo(start_loc.row, start_loc.col);
	// set all locations to have an event for clicking them to move
	for(const key in mapdata.locations) {
		let loc = mapdata.locations[key];
		let locData = getLocationGraphics(loc.y, loc.x);
		locData.location.addEventListener("click", () => {
			moveTo(locData.row, locData.col);
		});
	}
}

function validPath(row1, col1, row2, col2) {
	for(const key in mapData.doors) {
		let door = mapData.doors[key]
		if(door.start.row == row1 && door.start.col == col1 &&
			door.end.row == row2 && door.end.col == col2) {
			return true;
		}
		if(door.end.row == row1 && door.end.col == col1 &&
			door.start.row == row2 && door.start.col == col2 &&
			door.twoway) {
			return true;
		}
	}
	return false;
}

function moveTo(row, col) {
	if(player.location.row != null && player.location.col != null) {
		if(!validPath(player.location.row, player.location.col, row, col)) {
			console.log('ERROR: Invalid path!')
			return;
		}
		let start = getLocationGraphics(player.location.row, player.location.col);
		start.location.style.fill = "yellow";
	}
	let end = getLocationGraphics(row, col);
	end.location.style.fill = "red";
	player.location.row = end.row;
	player.location.col = end.col;
}
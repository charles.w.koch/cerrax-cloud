
import logging

from django.views import View
from django.forms.models import model_to_dict
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy

from core.models import Apps, CerraxUser
from core.views import BaseView

#-== @h1
# Arkham Views
#-== /arkham.views.py
#________________________________________


##########################################
#-== @class
class ArkhamView(BaseView):
	#-== Sets the /app_permission for all derived views as /Apps.ARKHAM .

	app_permission = Apps.ARKHAM
	

##########################################
#-== @class
class TestView(ArkhamView):
	#-== Test page used for various dev testing

	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Test page used for various dev testing

	def get(self, request, *args, **kwargs):
		return render(request, 'arkham/map_TEST.html', self.context)
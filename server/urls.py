"""cerrax-cloud URL Configuration
"""
from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect

def redirect_view(request, uri, root=''):
    return redirect(root+uri, permanent=True)

urlpatterns = [

    # admin interface !! FOR DEV ONLY - NOT PRODUCTION !!
    path('admin/', admin.site.urls),

    # main website (Cerrax.com)
    path('', include(('website.urls', 'website'), namespace='website')),

    # Cerrax Cloud apps
    path('cloud/', include(('core.urls', 'core'), namespace='core')),
    path('cloud/lister/', include(('lister.urls', 'lister'), namespace='lister')),
    path('cloud/docket/', include(('docket.urls', 'docket'), namespace='docket')),
    path('cloud/arkham/', include(('arkham.urls', 'arkham'), namespace='arkham')),
    # redirect lister, since it it used frequently
    # deprecate this path and eventually disable it
    path('lister/<path:uri>', redirect_view, {'root':'/cloud/lister/'}),
    
]

# Cerrax Cloud

A Django server powering Cerrax applications.

## Development

A helper script is provided to assist with development: ```cxcloud.sh```.

It may be helpful to alias this script for ease of use:
```
alias cx="./cxcloud.sh"
```

For information about the commands available in this script, use the "help" command:
```
./cxcloud.sh help
```

### Setting up Dev Environment

You should always do Python development in a virtual environment. Make sure you set up and activate a virtual environment before doing these steps.

**Using the ```cxcloud.sh``` script:**

In the root of the repo, you can use the ```devsetup``` command to quickly set up a dev environment.

```
./cxcloud.sh devsetup
```

**Manually creating the dev environment:**

If you cannot use the ```cxcloud.sh``` script, you can also do the same steps as the script manually.

1. Confirm that you have a valid Python version (3.7 - 3.10) installed
2. Confirm that you have ```pip3``` installed
3. Install dependencies from the ```requirements.txt``` file
  - ```pip3 install -r requirements.txt```
4. Create a settings file: ```server/settings.py```

## Deployment

The ```cxcloud.sh``` script provides commands which help prepare an environment for deploy.

* ```prodsetup``` : Configures the environment to run the Django server. Run this once in order to prepare the prodction environment.
* ```predeploy``` : Executes steps to prepare the code for deployment. This command should be run every time changes have been made to the code or other files.
* ```backup``` : Makes a backup of the database. This command is always executed during the ```predeploy``` command.

### Deploying on in-house server

Cerrax Cloud is being moved to our in-house server.
As such, the deployment is slightly different from the previous (PythonAnywhere).

1. Log into the server
2. Navigate to the directory and activate the virtual environment
  - ```cd /var/www/cerrax-cloud && source .venv/bin/activate```
3. Pull changes from GitLab
  - ```git pull```
  - You'll need the username and password/token to access the repo
4. If there are other steps needed before deployment, do them as well
  - This could be steps such as adding or changing values in the settings.py or adding modifying other production-only files
5. Run the ```predeploy``` command to prepare for deployment
  - ```sudo ./cxcloud.sh predeploy```
6. Restart the apache server, which will reload the Django app
  - ```sudo systemctl reload apache2```
7. Confirm site is up and changes have been applied
8. Clean up
  - Remove any old/unecessary files or database backups
9. Log out of the server

### Deploying on PythonAnywhere (legacy)

The current Cerrax Cloud production is deployed on PythonAnywhere, a Python application hosting service.
Below are the steps to deploy a change in that production environment.

1. Log in to PythonAnywhere
  - https://www.pythonanywhere.com/login/
2. Go to the Consoles tab and launch a Bash terminal
3. In the terminal, activate the virtual environment
  - ```workon virtenv```
4. Pull changes from GitLab
  - ```git pull```
  - You'll need the username and password/token to access the repo
5. If there are other steps needed before deployment, do them as well
  - This could be steps such as adding or changing values in the settings.py or adding modifying other production-only files
6. Run the ```predeploy``` command to prepare for deployment
  - ```./cxcloud.sh predeploy```
7. Go to the "Web" tab and click the "reload" button to load the changes
8. Confirm site is up and changes have been applied
9. Clean up
  - Remove any old/unecessary files or database backups
  - Shut down the Bash terminal
10. Log out of PythonAnywhere

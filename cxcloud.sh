#!/usr/bin/env bash

generate_secret_key() {
	echo $(python3 -c 'import secrets; print(secrets.token_hex(100))')
}

generate_settings_file() {
	if [ -f server/settings.py ]
	then
		echo "ERROR: settings.py already exists"
		exit 1
	fi
	SECRET_KEY=$(generate_secret_key)
	if [ $1 == "prod" ]
	then
		prod_settings_file $SECRET_KEY
	else
		dev_settings_file $SECRET_KEY
	fi
}

dev_settings_file() {
	cat > server/settings.py << EOT
# These settings are for DEVELOPMENT ONLY!
# !!!-- DO NOT USE THIS IN PRODUCTION --!!!

from server.base_settings import *

SECRET_KEY = '$1'

DEBUG = True

# When DEBUG is True and this is empty, any IP address will be allowed.
ALLOWED_HOSTS = []
EOT
}

prod_settings_file() {
	cat > server/settings.py << EOT
from server.base_settings import *

SECRET_KEY = '$1'

ALLOWED_HOSTS = []
EOT
}

env_setup() {
	VERSION=$(python3 --version | grep -E "Python 3\.([7-9]|10)")
	if [ -z "$VERSION" ]
	then
		echo "ERROR: No valid Python version available"
		exit 1
	fi
	pip3 install -r requirements.txt
	if [ $? != 0 ]
	then
		echo "ERROR: Could not install required dependencies"
		exit 1
	fi
}

dev_setup() {
	echo "It is highly recommended that you use a virtual environment for development."
	echo "Please make sure you have activated your virtual environment before executing this command."
	echo ""
	read -p "Do you wish to continue? (y/n): " res
	if [ "$res" == "N" ] || [ "$res" == "n" ]
	then
		exit 1
	fi

	env_setup
	if [ $? != 0 ]
	then
		exit 1
	fi
	generate_settings_file "dev"
	if [ $? != 0 ]
	then
		exit 1
	fi
}

prod_setup() {
	env_setup
	if [ $? != 0 ]
	then
		exit 1
	fi
	generate_settings_file "prod"
	if [ $? != 0 ]
	then
		exit 1
	fi

	echo "NOTE: settings.py file has been created with a unique SECRET KEY."
	echo "Make sure to add/edit relevant settings before doing pre-deply or deployment."
}

generate_docs() {
	python3 manage.py generate_docs
}

check() {
	python3 manage.py makemigrations --dry-run --check
	if [ $? != 0 ]
	then
		echo "ERROR: There are model changes which do not have migrations"
		exit 1
	fi
}

commit() {
	if [ -z "$1" ]
	then
		echo "ERROR: No commit message provided"
		exit 1
	fi
	PUSH=false
	ADD_ALL=true
	COMMIT_MSG=$1
	# check for missing migrations
	check
	if [ $? != 0 ]
	then
		exit 1
	fi
	# generate documentation
	generate_docs
	git add DOCUMENTATION.html
	if [ $ADD_ALL = true ]
	then
		git add -A
	fi
	# commit (and optionally push)
	git commit -m "$COMMIT_MSG"
	if [ $PUSH = true ]
	then
		git push
	fi
}

build() {
	generate_docs
	python3 manage.py makemigrations
	#python3 manage.py makemessages --all
	#python3 manage.py compilemessages
}

backup_db() {
	DB_FILE="db.sqlite3"
	if [ -z $1 ]
	then
		DB_FILE="db.sqlite3"
	else
		DB_FILE=$1
	fi
	cp "$DB_FILE" db-$(date "+%Y-%m-%d__%H-%M-%S").bak
}

predeploy() {
	backup_db
	python3 manage.py migrate
	python3 manage.py collectstatic
}

if [ $1 = 'devsetup' ]
then
	dev_setup

elif [ $1 = 'gensettings' ]
then
	generate_settings_file $2

elif [ $1 = 'docs' ]
then
	generate_docs

elif [ $1 = 'check' ]
then
	check

elif [ $1 = 'run' ]
then
	python3 manage.py runserver "${@: 2}"

elif [ $1 = 'manage' ]
then
	python3 manage.py "${@: 2}"

elif [ $1 = 'commit' ]
then
	commit "$2"

elif [ $1 = 'build' ]
then
	build

elif [ $1 = 'prodsetup' ]
then
	prod_setup

elif [ $1 = 'backup' ]
then
	backup_db $2

elif [ $1 = 'predeploy' ]
then
	predeploy

elif [ $1 = 'help' ]
then
	less cxcloudhelp.txt

else
	echo "Invaid command usage. Use './cxcloud.sh help' for more information"
fi




from django.db import models

from core.models import CoreModel, CerraxUser
from core.utils import Sort, Sorting

#-== @h1
# Lister Models
#-== /lister.models.py
#________________________________________



##########################################
#-== @class
class ListSorting(Sorting):
	#-== Sortings available for the /List model.

	DATA = [
		Sort('name', 'Sort by Name'),
		Sort('created', 'Sort by Created'),
	]



##########################################
#-== @class
class ListItemSorting(Sorting):
	#-== Sortings available for the /ListItem model.

	DATA = [
		Sort('name', 'Sort by Name'),
		Sort('created', 'Sort by Created'),
		Sort('checked', 'Sort by Marked'),
	]



##########################################
#-== @class
class ListerSettings(CoreModel):
	#-== User-specific settings to tailor the experience.

	# -== *Model Fields:*
	# @deflist
	# user: the /CerraxUser the settings belong to
	# purge_old_items_days: number of days after a /ListItem
	#						has been marked that it will remain in the list.
	#						Once this age has been surpassed,
	#						the ListItem is permanently deleted
	# auto_delete_empty_list: a boolean indicating if an empty list
	#						should be deleted automatically
	# list_sorting: the default sorting of the list

	user = models.ForeignKey(CerraxUser, on_delete=models.CASCADE)
	purge_old_items_days = models.IntegerField(default=7)
	auto_delete_empty_list = models.BooleanField(default=True)
	list_sorting = models.CharField(max_length=30, choices=ListSorting.choices(), default='created')

	#-== @note
	# The /list_sorting is automatically changed and stored
	# whenever the user touches the sort buttons in the button bar.
	# This ensures that the entries always appear
	# in the same order that the user last left them.

	def __str__(self):
		return '{} settings'.format(self.user)



##########################################
#-== @class
class List(CoreModel):
	#-== The main object that /ListItem objects are contained within.

	#-== *Model Fields:*
	# @deflist
	# user: the /CerraxUser the list belongs to
	# name: the name of the list
	# show_checked: a boolean indicating if marked items should appear in the list
	# sorting: the default sorting of items in the list

	user = models.ForeignKey(CerraxUser, on_delete=models.CASCADE)
	name = models.CharField(max_length=100)
	show_checked = models.BooleanField(default=False)
	sorting = models.CharField(max_length=30, choices=ListItemSorting.choices(), default='created')

	#-== @note
	# The /show_checked and /sorting are automatically changed and stored
	# whenever the user touches tthose buttons in the button bar.
	# This ensures that the entries always appear
	# exactly the way that the user last left them.

	def __str__(self):
		return "{} - {}".format(self.user, self.name)



##########################################
#-== @class
class ListItem(CoreModel):
	#-== An individual item to be marked in a /List .

	#-== *Model Fields:*
	# @deflist
	# parent: the /List object this item belongs to
	# name: the name of the item
	# checked: the datetime that the item was checked.
	#			Set to /None when it has been unchecked.

	parent = models.ForeignKey(List, on_delete=models.CASCADE)
	name = models.CharField(max_length=100, blank=True, null=True)
	checked = models.DateTimeField(blank=True, null=True)

	def __str__(self):
		return '{} - {}'.format(self.parent, self.name)


# LIST SHARE SETTINGS
# Links non-owner users to a list and
# stores their settings and permissions
##########################################
#-== @class
class ListShare(CoreModel):
	#-== Links non-owner users to a list and
	# stores their settings and permissions.

	#-== *Model Fields:*
	# @deflist
	# parent: the /List object this share refers to
	# user: the /CerraxUser the list is being shared with
	# view_items: a blooean which indicates if the /user
	#				can see the items in a list.
	#				This should always be set to /True
	# edit_items: a boolean which indicates if the /user
	#				can add/mark/delete items from the list
	# edit_list: a boolean which indicates if the /user
	#				can change the name of the list,
	#				or delete the list

	parent = models.ForeignKey(List, on_delete=models.CASCADE)
	user = models.ForeignKey(CerraxUser, on_delete=models.CASCADE)
	view_items = models.BooleanField(default=True)
	edit_items = models.BooleanField(default=True)
	edit_list = models.BooleanField(default=False)
	show_checked = models.BooleanField(default=False)
	sorting = models.CharField(max_length=30, choices=ListItemSorting.choices(), default='created')

	def __str__(self):
		return '{} - share with {}'.format(self.parent, self.user)






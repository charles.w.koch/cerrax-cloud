
import logging
import datetime

from django.utils import timezone
from django.views import View
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.shortcuts import render
from django.urls import reverse_lazy
from django.db.models import Q
from django.db.models.functions import Lower

from core.models import Apps, AppPermissions, CerraxUser
from core.views import BaseView, CrudMixin
from lister.models import ListSorting, ListItemSorting, ListerSettings, List, ListItem, ListShare


#-== @h1
# Lister Views
#-== /lister.views.py
#________________________________________


##########################################
#-== @class
class ListerBase(BaseView):
	#-== Sets the /app_permission for all derived views as /Apps.LISTER .

	app_permission = Apps.LISTER

	# ----------------------------------------------------
	#-== @method
	def is_valid_user(self, list, user):
		#-== Check if the user is the owner of the list or a superuser
		# @params
		# list: the /List object to check against
		# user: the /CerraxUser object to compare
		# @returns
		# /True if the /user is valid for the /list, /False if not.
		# @note
		# A superuser ( /user.is_superuser ) will always return /True from this method.

		return user == list.user or user.is_superuser

	# ----------------------------------------------------
	#-== @method
	def has_list_permissions(self, list, user, list_settings):
		#-== Check if the user has permission to edit the list
		# @params
		# list: the /List object to check against
		# user: the /CerraxUser object to compare
		# list_settings: the /ListShare object for the list and /user
		# @returns
		# /True if the /user has valid permissions for the /list, /False if not.
		# @note
		# A superuser ( /user.is_superuser ) will always return /True from this method.

		return user == list.user \
			or user.is_superuser \
			or (user != list.user
				and list_settings != list
				and list_settings.edit_list)

	# ----------------------------------------------------
	#-== @method
	def has_list_edit(self, user, list_settings):
		#-== Check if the user has permission to edit the list
		# @params
		# user: the /CerraxUser object to compare
		# list_settings: the /ListShare object for the list and /user
		# @returns
		# /True if the /user has the edit permission for the /list, /False if not.
		# @note
		# A superuser ( /user.is_superuser ) will always return /True from this method.

		return list_settings.edit_items or user.is_superuser



##########################################
#-== @class
class ListerHome(ListerBase):
	#-== Shows all of the lists available to the user.
	# This includes not only lists that the user has created,
	# but also any lists that have been shared with them.

	page_title = 'Lister'
	back_url = reverse_lazy('core:landing')
	button_bar_left = ListSorting.button_bar_display()
	button_bar_right = [
		{'display': '+ New List', 'url': reverse_lazy('lister:create-list') },
		{'display': 'Settings', 'url': reverse_lazy('lister:settings') },
	]

	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Display the lists available to the user.
	# @params
	# sort: the field name by which the lists should be sorted

	def get(self, request, *args, **kwargs):
		sorting = request.GET.get('sort', None)

		user_settings, created = ListerSettings.objects.get_or_create(user=request.user)
		if sorting:
			user_settings.list_sorting = sorting
			user_settings.save()
		sorting = user_settings.list_sorting

		# housekeeping
		self.purge_old_items(request.user, user_settings)

		listlist = List.objects.filter(Q(user=request.user) | Q(listshare__user=request.user))
		if sorting:
			listlist = listlist.order_by(Lower(sorting)).distinct()

		self.context['listlist'] = listlist
		return render(request, 'lister/home.html', self.context)


	# ----------------------------------------------------
	#-== @method
	def purge_old_items(self, user, settings):
		#-== Every time the view is requested, this method checks
		# for old /ListItem objects and empty /List objects,
		# and deletes them according to the user's settings.
		# @params
		# user: the /CerraxUser viewing the page
		# settings: the /ListerSettings of the /user

		purge_days = settings.purge_old_items_days
		if purge_days >= 0:
			purge_threshold = timezone.now() - datetime.timedelta(days=purge_days)
			old_items = ListItem.objects.filter(parent__user=user, checked__lt=purge_threshold)
			num_deleted = old_items.delete()[0]
			if num_deleted > 0:
				self.logger.info('Purged {} items older than {} days'.format(num_deleted, purge_days))
		if settings.auto_delete_empty_list:
			non_empty_lists = ListItem.objects.filter(parent__user=user).values_list('parent__id').distinct()
			empty_lists = List.objects.filter(user=user).exclude(pk__in=non_empty_lists)
			num_deleted = empty_lists.delete()[0]
			if num_deleted > 0:
				self.logger.info('Purged {} empty lists'.format(num_deleted))



##########################################
#-== @class
class ListerSettingsPage(ListerBase):
	#-== A page to adjust the /ListerSettings of the user.

	page_title = 'Settings'
	back_url = reverse_lazy('lister:home')

	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Display the user's current settings within the /lister\/settings.html template.

	def get(self, request, *args, **kwargs):
		user_settings, created = ListerSettings.objects.get_or_create(user=request.user)
		self.context['auto_delete_empty_list'] = user_settings.auto_delete_empty_list
		days = user_settings.purge_old_items_days
		self.context['purge_options'] = [
			{ 'value': 0, 'display': 'immediately', 'selected': days == 0 },
			{ 'value': 1, 'display': 'after 1 day', 'selected': days == 1  },
			{ 'value': 3, 'display': 'after 3 days', 'selected': days == 3  },
			{ 'value': 7, 'display': 'after 1 week', 'selected': days == 7  },
			{ 'value': 30, 'display': 'after 1 month', 'selected': days == 30  },
			{ 'value': 90, 'display': 'after 3 months', 'selected': days == 90  },
			{ 'value': -1, 'display': 'never', 'selected': days == -1  },
		]

		return render(request, 'lister/settings.html', self.context)

	# ----------------------------------------------------
	#-== @method
	# POST
	#-== Adjust the settings based on the data received from the form.
	# @params
	# purge_old_items_days: the number of days a marked item
	#						remains in a list before being deleted
	# auto_delete_empty_list: a boolean indicating if empty lists
	#						should automatically be deleted

	def post(self, request, *args, **kwargs):
		user_settings, created = ListerSettings.objects.get_or_create(user=request.user)
		purge_days = request.POST.get('purge_old_items_days', None)
		if purge_days is not None:
			user_settings.purge_old_items_days = purge_days
		auto_delete = request.POST.get('auto_delete_empty_list', False)
		if auto_delete:
			user_settings.auto_delete_empty_list = True
		else:
			user_settings.auto_delete_empty_list = False
		user_settings.save()

		return self.redirect('lister:home')



##########################################
#-== @class
class CreateList(ListerBase):
	#-== Create a new list.

	page_title = 'New List'
	back_url = reverse_lazy('lister:home')

	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Display the /lister\/create-list.html template.

	def get(self, request, *args, **kwargs):
		return render(request, 'lister/create-list.html', self.context)

	# ----------------------------------------------------
	#-== @method
	# POST
	#-== Create the /List and /ListItem objects.
	# On success, redirects to /ListerHome .
	# @params
	# name: the name of the list *(required)*
	# show_checked: if this parameter is present, regardless of its value,
	#				the list will display marked items
	# item1: the first item name to display in the list
	# item2: the second item name to display in the list
	# item3: the third item name to display in the list
	# item4: the fourth item name to display in the list
	# item5: the fifth item name to display in the list
	# item6: the sixth item name to display in the list
	# item7: the seventh item name to display in the list
	# item8: the eighth item name to display in the list
	# item9: the ninth item name to display in the list
	# item10: the tenth item name to display in the list

	def post(self, request, *args, **kwargs):
		name = request.POST.get('name', None)
		show_checked = request.POST.get('show_checked', None) is not None

		if not name:
			self.log_error('You must name the list.')

		list_items = []
		for i in range(10):
			item = request.POST.get('item'+str(i+1), None)
			if item:
				list_items.append(item)

		if not list_items:
			self.log_error('You must include at least 1 list item.')

		if self.context['errors']:
			return render(request, 'lister/create-list.html', self.context)

		newlist = List.objects.create(user=request.user, name=name, show_checked=show_checked)
		for item in list_items:
			ListItem.objects.create(parent=newlist, name=item)

		if not self.context['errors']:
			return self.redirect('lister:home')
		else:
			return render(request, 'lister/create-list.html', self.context)



##########################################
#-== @class
class ViewList(ListerBase):
	#-== Display a list and allow adding items
	# and marking items when they are completed.

	page_title = 'View List'
	back_url = reverse_lazy('lister:home')
	button_bar_left = ListItemSorting.button_bar_display()

	# ----------------------------------------------------
	#-== @method
	def display_list(self, user, thelist, sorting=None, show_checked=None):
		#-== Determines the settings for displaying the list and then
		# prepares the context to deliver to the template.

		list_settings = thelist
		if user != thelist.user:
			try:
				list_settings = ListShare.objects.get(parent=thelist, user=user)
			except ObjectDoesNotExist:
				if not user.is_superuser:
					self.log_error('You you do not have permission to access this list.')
					return self.redirect('lister:home')

		if sorting is not None:
			list_settings.sorting = sorting
			list_settings.save()
		sorting = list_settings.sorting

		if show_checked is not None:
			show_checked = show_checked in ['true', 'True']
			list_settings.show_checked = show_checked
			list_settings.save()
		show_checked = list_settings.show_checked
		
		self.context['page_title'] = thelist.name
		self.context['show_checked'] = list_settings.show_checked
		button_display = 'Show Marked'
		button_value = 'true'
		if show_checked:
			button_display = 'Hide Marked'
			button_value = 'false'
		self.context['button_bar_right'] = [
			{'display': button_display, 'url': '?show_checked='+button_value },
		]

		if self.has_list_permissions(thelist, user, list_settings):
			self.context['button_bar_right'].append({'display': 'Edit List', 'url': reverse_lazy('lister:edit-list', args=[thelist.id]) })
			self.context['items_editable'] = True

		queryset = thelist.listitem_set.all()
		if not show_checked:
			queryset = queryset.filter(checked__isnull=True)
		queryset = queryset.order_by(Lower(sorting))
		self.context['list_items'] = queryset


	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Prepares the list and displays it in the /lister\/view-list.html template.
	# Any changes made to sorting or the Show Marked option will be saved.
	# @params
	# sort: the field name by which the items should be sorted
	# show_checked: if /True , marked items will be displayed in the list

	def get(self, request, pk, *args, **kwargs):
		sorting = request.GET.get('sort', None)
		show_checked = request.GET.get('show_checked', None)
		thelist = List.objects.get(pk=pk)

		self.display_list(request.user, thelist, sorting, show_checked)
		return render(request, 'lister/view-list.html', self.context)

	# ----------------------------------------------------
	#-== @method
	# POST
	#-== Adds a new item to the list based on the parameters received.
	# Then prepares the list and displays it in the /lister\/view-list.html template.
	# @params
	# newitem: the name of the item to add to the list *(required)*

	def post(self, request, pk, *args, **kwargs):
		thelist = List.objects.get(pk=pk)
		newitem = request.POST.get('newitem', None)

		list_settings = None
		if request.user != thelist.user:
			try:
				list_settings = ListShare.objects.get(parent=thelist, user=request.user)
				if not list_settings.edit_items and not request.user.is_superuser:
					self.log_error(request.user.username + ' - You do not have permission to edit this list.')
					return self.redirect('lister:home')
			except ObjectDoesNotExist:
				if not request.user.is_superuser:
					self.log_error(request.user.username + ' - You do not have permission to edit this list.')
					return self.redirect('lister:home')

		if newitem:
			ListItem.objects.create(parent=thelist, name=newitem)

		self.display_list(request.user, thelist)
		return render(request, 'lister/view-list.html', self.context)



##########################################
#-== @class
class ToggleListItem(ListerBase):
	#-== Sets a /ListItem object's /checked field based upon its previous status.

	# ----------------------------------------------------
	#-== @method
	# GET
	# -== If the /ListItem does not have a checked value,
	# this method sets it to the current datetime.
	# If the /ListItem has a value in /checked ,
	# this method sets it to /None .

	def get(self, request, pk, *args, **kwargs):
		item = ListItem.objects.get(pk=pk)
		parent = item.parent

		list_settings = None
		if request.user != parent.user:
			try:
				list_settings = ListShare.objects.get(parent=parent, user=request.user)
				if not self.has_list_edit(request.user, list_settings):
					self.log_error(request.user.username + ' - You do not have permission to edit this list.')
					return self.redirect('lister:view-list', parent.id)
			except ObjectDoesNotExist:
				if not request.user.is_superuser:
					self.log_error(request.user.username + ' - You do not have permission to edit this list.')
					return self.redirect('lister:view-list', parent.id)

		if item.checked is None:
			item.checked = timezone.now()
		else:
			item.checked = None
		item.save()

		return self.redirect('lister:view-list', item.parent.id)



##########################################
#-== @class
class EditList(ListerBase):
	#-== Allow a user to edit the name of a list and
	# permanently delete items from the list.

	page_title = 'Edit List'
	back_url = reverse_lazy('lister:home')

	# ----------------------------------------------------
	#-== @method
	def display_list(self, user, thelist, sorting=None, show_checked=None):
		#-== Determines the settings for displaying the list and then
		# prepares the context to deliver to the template.

		self.context['back_url'] = reverse_lazy('lister:view-list', args=[thelist.id])

		list_settings = thelist
		if user != thelist.user:
			try:
				list_settings = ListShare.objects.get(parent=thelist, user=user)
			except ObjectDoesNotExist:
				if not user.is_superuser:
					self.log_error(user.username + ' - You do not have permission to edit this list.')
					return self.redirect('lister:view-list', thelist.id)

		self.context['button_bar_right'] = [
			{'display': 'Share List', 'url': reverse_lazy('lister:share-list', args=[thelist.id]) },
		]

		self.context['listid'] = thelist.id

		if sorting is not None:
			list_settings.sorting = sorting
			list_settings.save()
		sorting = list_settings.sorting

		if show_checked is not None:
			show_checked = show_checked in ['true', 'True']
			list_settings.show_checked = show_checked
			list_settings.save()
		show_checked = list_settings.show_checked
		
		self.context['name'] = thelist.name
		self.context['show_checked'] = list_settings.show_checked

		queryset = thelist.listitem_set.all().order_by(Lower(sorting))
		self.context['list_items'] = queryset


	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Displays the form to edit the list via the /lister\/edit-list.html template.
	# If the list has been shared from another user,
	# this method determines which parts of the form
	# are accessible via the user's permissions.

	def get(self, request, pk, *args, **kwargs):
		thelist = List.objects.get(pk=pk)

		list_settings = None
		if request.user != thelist.user:
			try:
				list_settings = ListShare.objects.get(parent=thelist, user=request.user)
				if not self.has_list_edit(request.user, list_settings):
					self.log_error(request.user.username + ' - You do not have permission to edit this list.')
					return self.redirect('lister:view-list', thelist.id)
			except ObjectDoesNotExist:
				if not request.user.is_superuser:
					self.log_error(request.user.username + ' - You do not have permission to edit this list.')
					return self.redirect('lister:view-list', thelist.id)

		if self.has_list_permissions(thelist, request.user, list_settings):
			self.context['items_editable'] = True

		self.display_list(request.user, thelist)
		return render(request, 'lister/edit-list.html', self.context)

	# ----------------------------------------------------
	#-== @method
	# POST
	#-== Updates the list based on the data supplied from the form.
	# If the list has been shared from another user,
	# this method determines which parts of the form
	# are accessible via the user's permissions.
	# Upon success, this method redirects to the /ViewList view.
	# @params
	# name: the name of the list *(required)*
	# show_checked: if this parameter is present, regardless of its value,
	#				the list will display marked items

	def post(self, request, pk, *args, **kwargs):
		thelist = List.objects.get(pk=pk)
		name = request.POST.get('name', None)
		show_checked = request.POST.get('show_checked', None) is not None

		list_settings = None
		if request.user != thelist.user:
			try:
				list_settings = ListShare.objects.get(parent=thelist, user=request.user)
				if not self.has_list_edit(request.user, list_settings):
					self.log_error(request.user.username + ' - You do not have permission to edit this list.')
					return self.redirect('lister:view-list', thelist.id)
			except ObjectDoesNotExist:
				if not request.user.is_superuser:
					self.log_error(request.user.username + ' - You do not have permission to edit this list.')
					return self.redirect('lister:view-list', thelist.id)

		if self.has_list_permissions(thelist, request.user, list_settings):
			self.context['items_editable'] = True

		if not name:
			self.log_error('You must name the list.')

		if self.context['errors']:
			return render(request, 'lister/edit-list.html', self.context)

		thelist.name = name
		thelist.show_checked = show_checked
		thelist.save()

		self.display_list(request.user, thelist)
		return self.redirect('lister:view-list', thelist.id)



##########################################
#-== @class
class DeleteItem(ListerBase):
	#-== Deletes an item from a list.
	
	# ----------------------------------------------------
	#-== @method
	# POST
	#-== Deletes an item from a list.

	def post(self, request, pk, *args, **kwargs):
		item = ListItem.objects.get(pk=pk)
		parent = item.parent

		list_settings = None
		if request.user != parent.user:
			try:
				list_settings = ListShare.objects.get(parent=parent, user=request.user)
				if not self.has_list_edit(request.user, list_settings):
					self.log_error(request.user.username + ' - You do not have permission to edit this list.')
					return self.redirect('lister:home')
			except ObjectDoesNotExist:
				if not request.user.is_superuser:
					self.log_error(request.user.username + ' - You do not have permission to edit this list.')
					return self.redirect('lister:home')

		item.delete()

		return self.redirect('lister:edit-list', parent.id)



##########################################
#-== @class
class DeleteList(ListerBase):
	#-== Displays a confirmation window before deleting an entire list.

	page_title = 'Delete List'

	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Displays a confirmation window via the /lister\/delete-list.html template.

	def get(self, request, pk, *args, **kwargs):

		thelist = List.objects.get(pk=pk)
		if request.user != thelist.user and not request.user.is_superuser:
			self.log_error(request.user.username + ' - Only the list owner can delete a list.')
			return self.redirect('lister:edit-list', pk)

		thelist = List.objects.get(pk=pk)
		self.context['back_url'] = reverse_lazy('lister:edit-list', args=[thelist.id])

		self.context['list_name'] = thelist.name
		num_items = thelist.listitem_set.all().count()
		s = 's'
		if num_items == 1:
			s = ''
		numstr = '{} item{}'.format(num_items, s)
		self.context['num_items'] = numstr

		return render(request, 'lister/delete-list.html', self.context)
	
	# ----------------------------------------------------
	#-== @method
	# POST
	#-== Deletes the list.

	def post(self, request, pk, *args, **kwargs):
		thelist = List.objects.get(pk=pk)
		if not self.is_valid_user(thelist, request.user):
			self.log_error(request.user.username + ' - Only the list owner can delete a list.')
			return self.redirect('lister:edit-list', pk)
		thelist.delete()

		return self.redirect('lister:home')



##########################################
#-== @class
class ShareList(ListerBase):
	#-== Provides a way to share a list with other users.
	# Individual permission can be assigned as to
	# who can view the list, edit the list, or add/mark/delete items in the list.

	page_title = 'Share List'

	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Displays all users who have access to the Lister app
	# and allows the user to assign permissions for each user they share the list with.

	def get(self, request, pk, *args, **kwargs):
		thelist = List.objects.get(pk=pk)
		if not self.is_valid_user(thelist, request.user):
			self.log_error(request.user.username + ' - Only the list owner can change sharing settings.')
			return self.redirect('lister:edit-list', pk)

		self.context['back_url'] = reverse_lazy('lister:edit-list', args=[thelist.id])

		self.context['list_name'] = thelist.name
		num_items = thelist.listitem_set.all().count()
		s = 's'
		if num_items == 1:
			s = ''
		numstr = '{} item{}'.format(num_items, s)
		self.context['num_items'] = numstr

		lister_perms = AppPermissions.objects.filter(app=Apps.LISTER).values_list('user__id').distinct()
		print(lister_perms)
		allusers = CerraxUser.objects.exclude(pk=thelist.user.id).filter(pk__in=lister_perms).order_by(Lower('first_name'))
		user_list = []

		for user in allusers:
			record = {'user': user }
			try:
				listshare = ListShare.objects.get(parent=thelist, user=user)
				record['view_items'] = listshare.view_items
				record['edit_items'] = listshare.edit_items
				record['edit_list'] = listshare.edit_list
			except ObjectDoesNotExist:
				record['view_items'] = False
				record['edit_items'] = False
				record['edit_list'] = False
			user_list.append(record)

		self.context['user_list'] = user_list

		return render(request, 'lister/share-list.html', self.context)

	# ----------------------------------------------------
	#-== @method
	# POST
	#-== Assigns permissions for users who have been added to the share of the list.
	# Also removes any sharing permissions for users who were not included in the data.

	#-== For each user who has been indcated as having some kind of share permissions,
	# the parameters received will be in the form: /<user ID>-<permission_name>

	#-== *Example:*
	# @deflist
	# 3-view_items: User ID 3, enable /view_items permission

	#-== @note
	# If a user does not have the /view_items permission set,
	# then none of the other share permissions will take effect.
	# Any user who has been added to the share *must* have
	# the /view_items permission.

	def post(self, request, pk, *args, **kwargs):
		thelist = List.objects.get(pk=pk)

		list_settings = None
		if not self.is_valid_user(thelist, request.user):
			self.log_error(request.user.username + ' - Only the list owner can change sharing settings.')
			return self.redirect('lister:edit-list', pk)

		# assemble a list of users and their permissions
		data = {}
		for key, val in request.POST.items():
			if '-' in key:
				datasplit = key.split('-')
				userid = datasplit[0]
				datakey = datasplit[1]
				if userid not in data.keys():
					data[userid] = {
						'view_items': False,
						'edit_items': False,
						'edit_list': False,
					}
				data[userid][datakey] = True

		# geta list of all users
		lister_perms = AppPermissions.objects.filter(app=Apps.LISTER).values_list('user__id').distinct()
		allusers = CerraxUser.objects.exclude(pk=thelist.user.id).filter(pk__in=lister_perms).order_by(Lower('first_name'))

		for user in allusers:
			# if the user was in the data, we have something to set for them
			if str(user.id) in data.keys():
				if data[str(user.id)]['view_items']:
					listshare, created = ListShare.objects.get_or_create(parent=thelist, user=user)
					listshare.view_items = True
					listshare.edit_items = data[str(user.id)]['edit_items']
					listshare.edit_list = data[str(user.id)]['edit_list']
					listshare.save()
					self.logger.info('Added share settings for {}, list ID {}'.format(user.username, thelist.id))
				else:
					try:
						ListShare.objects.filter(user=user, parent=thelist).delete()
						self.logger.info('Deleted share settings for {}, list ID {}'.format(user.username, thelist.id))
					except ObjectDoesNotExist:
						# do nothing
						pass
			else:
				# if not in the data, we should destroy any permissions they had
				try:
					ListShare.objects.filter(user=user, parent=thelist).delete()
					self.logger.info('Deleted share settings for {}, list ID {}'.format(user.username, thelist.id))
				except ObjectDoesNotExist:
					# do nothing
					pass

		return self.redirect('lister:edit-list', thelist.id)









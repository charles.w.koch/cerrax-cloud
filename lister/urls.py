from django.urls import path, include

from lister import views

urlpatterns = [
    path('', views.ListerHome.as_view(), name = 'home'),
    path('settings/', views.ListerSettingsPage.as_view(), name = 'settings'),
    path('list/', views.CreateList.as_view(), name = 'create-list'),
    path('list/<int:pk>/', views.ViewList.as_view(), name = 'view-list'),
    path('list/<int:pk>/edit/', views.EditList.as_view(), name = 'edit-list'),
    path('list/<int:pk>/share/', views.ShareList.as_view(), name = 'share-list'),
    path('list/<int:pk>/delete/', views.DeleteList.as_view(), name = 'delete-list'),
    path('list/item/<int:pk>/toggle/', views.ToggleListItem.as_view(), name = 'toggle-item'),
    path('list/item/<int:pk>/delete/', views.DeleteItem.as_view(), name = 'delete-item'),
]
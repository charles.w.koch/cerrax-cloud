from django.urls import path, include

from core import views

urlpatterns = [
    path('login/', views.LoginPage.as_view(), name = 'login'),
    path('logout/', views.LogoutPage.as_view(), name = 'logout'),
    path('', views.LandingPage.as_view(), name='landing'),
    # Admin
    path('admin-home/', views.AdminHome.as_view(), name = 'admin-home'),
    path('user/', views.CreateUser.as_view(), name = 'user'),
    path('user/<int:pk>/', views.EditUser.as_view(), name = 'edit-user'),
    path('user/<int:pk>/delete/', views.DeleteUser.as_view(), name = 'delete-user'),
]

from django import template

from core.templatetags import icons

register = template.Library()

#-== @h1
# Core Custom Template Tags
#-== /core.templatetags.core_components.py
#________________________________________


#-== @method
# {% icon 'icon_name' %}
#-== Displays an icon from the Material Icons 2.2.0 library:
#								!https://fonts.google.com/icons

@register.inclusion_tag('components/icon.html')
def icon(name):
	iconstr = getattr(icons, name, None)
	return {'iconstr': iconstr}

import logging
from copy import deepcopy

from django.views import View
from django.forms.models import model_to_dict
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy

from core.models import Apps, ErrorMessage, CerraxUser, AppPermissions

#-== @h1
# Core Views
#-== /core.views.py
#________________________________________


##########################################
#-== @class
class UnauthenticatedView(View):
	#-== Provides the basic elements that all pages will use.
	# See the Django /View documentation for more info:
	#						!https://docs.djangoproject.com/en/3.2/topics/class-based-views/
	# @attributes
	# titlebar_visible: a boolean that determines if the title bar is visible
	# page_title: the title displayed in the titlebar of the app, as well as the browser tab
	# back_url: the URL for the Back button in the titlebar
	# menu_visible: boolean that determines if the menu is available in the titlebar
	# button_bar_left: a list of buttons to modify the view of the page
	# button_bar_right: a list of buttons which provide additional actions on the page

	titlebar_visible = False
	page_title = 'WARN: No Title'
	back_url = None
	menu_visible = True
	button_bar_left = []
	button_bar_right = []

	# ----------------------------------------------------
	#-== @method
	def log_error(self, error_msg, exception=None):
		# -== Handles settings error messages for both
		# the HTML output and the application logs.
		# @params
		# error_msg: the message to display
		# exception: the exception which should display a stack trace in the log

		# if the error_msg is a list of error messages,
		# log each message separately
		logerrorstr = error_msg
		if isinstance(error_msg, list):
			logerrorstr = '\n'.join(error_msg)
		# if there was an exception, log the traceback as well
		if exception:
			self.logger.exception(logerrorstr)
		else:
			self.logger.error(logerrorstr)
		# add the error_msg to the 'errors' context member
		if isinstance(error_msg, list):
			self.context['errors'].extend(error_msg)
		else:
			self.context['errors'].append(error_msg)


	# ----------------------------------------------------
	#-== @method
	def setup(self, request, *args, **kwargs):
		#-== Defines the context for the template render
		# and prepares the view for dispatch.

		super().setup(request, *args, *kwargs)
		self.logger = logging.getLogger('cxcloud.{}'.format(self.__class__.__qualname__))
		self.user = request.user
		self.context = {}
		self.context['errors'] = self.get_errors()
		self.context['titlebar_visible'] = self.titlebar_visible
		self.context['page_title'] = self.page_title
		self.context['back_url'] = self.back_url
		self.context['menu_visible'] = self.menu_visible
		self.context['button_bar_left'] = self.button_bar_left
		self.context['button_bar_right'] = self.button_bar_right

	# ----------------------------------------------------
	#-== @method
	def get_errors(self, user=None):
		#-== Collect any errors from database.
		# Error messages are deleted once they
		# are loaded from the database.
		# @params
		# user: the user to get errors for.
		#		If no user is provided, the view
		#		will attempt to find messages for
		#		the currently logged in user.

		error_list = []
		if not user:
			user = self.user

		if user and not user.is_anonymous:
			# if we have a user, retrieve any messages they have
			messages = ErrorMessage.objects.filter(user=user)
			for item in messages:
				error_list.append(item.message)
			# remove the messages, since they will be displayed
			messages.delete()

		return error_list

	# ----------------------------------------------------
	#-== @method
	def check_perms(self, request, *args, **kwargs):
		#-== Check permissions of the user. As this is
		# the unauthenticated view, nothing happens here.

		pass

	# ----------------------------------------------------
	#-== @method
	def initialize(self, request, *args, **kwargs):
		#-== Do any prep work before the view is dispatched.
		# This is blank and should be overriden as needed.

		pass

	# ----------------------------------------------------
	#-== @method
	def dispatch(self, request, *args, **kwargs):
		#-== Dispatches the view with the appropriate HTTP method
		# after checking permissions and initializing.

		self.check_perms(request, *args, **kwargs)
		self.initialize(request, *args, **kwargs)
		return super().dispatch(request, *args, **kwargs)

	#-== The Django /View class allows the developer
	# to create methods for each HTTP request method type
	# ( /GET , /POST , /PUT , /PATCH, /DELETE , /OPTIONS ).
	# Any methods which are not included will
	# respond with an HTTP error 405 (Method Not Allowed).

	# ----------------------------------------------------
	def get(self, request, *args, **kwargs):
		pass

	# ----------------------------------------------------
	# def post(self, request, *args, **kwargs):
	#	pass

	# ----------------------------------------------------
	# def put(self, request, *args, **kwargs):
	# 	pass

	# ----------------------------------------------------
	# def patch(self, request, *args, **kwargs):
	# 	pass

	# ----------------------------------------------------
	# def delete(self, request, *args, **kwargs):
	# 	pass

	# ----------------------------------------------------
	# def options(self, request, *args, **kwargs):
	# 	pass

	# ----------------------------------------------------
	#-== @method
	def redirect(self, to, *args, **kwargs):
		#-== An override for django's redirect which
		# makes sure we pass errors to the database
		# so that errors will be displayed upon redirect.

		err = self.context['errors']
		if 'errors' in kwargs.keys():
			err = kwargs.pop('errors')

		if self.user and not self.user.is_anonymous:
			for message in err:
				ErrorMessage.objects.create(message=message, user=self.user)

		return redirect(to, *args, **kwargs)



##########################################
#-== @class
class BaseView(LoginRequiredMixin, UnauthenticatedView):
	#-== Adds login reuirement and permission checks to basic page.
	# @attributes
	# login_url: the URL to redirect when an unauthenticed user makes a request
	# app_permission: the app name (derived from the /core.models.Apps class)
	#					which the user must have permission to access

	login_url = 'core:login'
	app_permission = None
	titlebar_visible = True

	# ----------------------------------------------------
	#-== @method
	def check_perms(self, request, *args, **kwargs):
		#-== Checks that the /request.user has the proper permissions.
		# Raises a /PermissionDenied exception if they do not.

		if self.app_permission and not request.user.has_app_permission(self.app_permission):
				raise PermissionDenied



##########################################
#-== @class
class LoginPage(UnauthenticatedView):
	#-== Provides a simple form for authentication.

	page_title = 'Login'

	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Renders the template /core\/login.html .

	def get(self, request, *args, **kwargs):
		return render(request, 'core/login.html', self.context)

	# ----------------------------------------------------
	#-== @method
	# POST
	#-== Authenticates and logs in the user.
	# Successful login redriects to the Landing page.

	def post(self, request, *args, **kwargs):
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username=username, password=password)
		if user:
			if user.is_active:
				login(request, user)
				return self.redirect('core:landing')
			else:
				self.logger.error("Disabled user attempted to log in: '{0}'".format(username))
				self.context['errors'].append('User has been disabled.')
		else:
			self.logger.error("Invalid login details for username '{0}'".format(username))
			self.context['errors'].append('Username and/or password is incorrect.')

		return render(request, 'core/login.html', self.context)



##########################################
#-== @class
class LogoutPage(BaseView):
	#-== Logs out a user and redirects to login.

	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Logs out a user and redirects to login.

	def get(self, request, *args, **kwargs):
		logout(request)
		return self.redirect('core:login')



##########################################
#-== @class
class LandingPage(BaseView):
	#-== The index of the server; provides a list of apps (based on the user's permissions).

	page_title = 'Cerrax Cloud'
	menu_visible = False

	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Displays the list of apps, based on the user's permissions.
	# Renders the /core\/landing.html template.

	def get(self, request, *args, **kwargs):
		mylist = []
		for record in Apps.list:
			if record['app'] == Apps.ADMIN:
				# Admin should always be at the bottom
				continue
			if request.user.has_app_permission(record['app']):
				mylist.append(record)

		userlist = []
		if request.user.has_app_permission(Apps.ADMIN):
			userlist.append({'app': Apps.ADMIN, 'display': 'Admin', 'url': 'core:admin-home'})

		userlist.append({'display': 'Logout', 'url': 'core:logout'})

		self.context['applist'] = mylist
		self.context['userlist'] = userlist
		return render(request, 'core/landing.html', self.context)



##########################################
#-== @class
class AdminHome(BaseView):
	#-== Provides a list of users and the ability to add and edit users.

	app_permission = Apps.ADMIN
	page_title = 'Admin'
	back_url = reverse_lazy('core:landing')
	button_bar_left = [
		{'display': 'Sort By Name', 'url': '?sort=first_name' },
		{'display': 'Sort By Created', 'url': '?sort=date_joined' },
	]
	button_bar_right = [
		{'display': '+ New User', 'url': reverse_lazy('core:user') },
		#{'display': 'Settings', 'url': reverse_lazy('core:admin-home') },
	]

	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Returns the list of users.
	# Renders the /core\/admin-home.html template.
	# @params
	# sort: the field name the users should be sorted by

	def get(self, request, *args, **kwargs):
		sorting = request.GET.get('sort', 'first_name')

		userlist = CerraxUser.objects.all()
		if sorting:
			userlist = userlist.order_by(sorting)

		self.context['userlist'] = userlist
		return render(request, 'core/admin-home.html', self.context)



##########################################
#-== @class
class CrudMixin:
	#-== A View-class mixin which provides methods
	# to simplify CRUD operations (create, read, update, delete).

	PYTHON = 'py'
	JSON = 'json'
	XML = 'xml'

	# ----------------------------------------------------
	#-== @method
	def serailize(self, obj, format='py', feilds=None):
		#-== Serializes an /obj into the provided /format with the indicated /fields .
		# @params
		# obj: the Django model object to serialize
		# format: a string constant to detrmine
		#			the output format ( /PYTHON , /JSON , or /XML )
		# fields: a list of field names to serialize.
		#			If /None , all model fields will be serialized

		if format == self.PYTHON:
			return model_to_dict(obj, fields=fields)
		elif format in [self.JSON, self.XML]:
			return serializers.serialize(format, obj, fields=fields)
		else:
			raise ValueError('Unsupported serialization format: {}'.format(format))

	# ----------------------------------------------------
	#-== @method
	def deserialize(self, data, format='py', modelclass=None):
		#-== Deserializes the /data from the provided /format into the provided /modelclass .
		# @params
		# data: the data to deserialize
		# format: a string constant to detrmine
		#			the input format ( /PYTHON , /JSON , or /XML )
		# modelclass: when using /PYTHON format, the model
		#			which the Python data shoudl be populated into
		#
		# -== Using /JSON or /XML formats will require the /data to be formatted
		# according to the standard Django serialization formats.
		# 	( !https://docs.djangoproject.com/en/3.2/topics/serialization/ )
		# When using /PYTHON format, the /modelclass must be provided
		# so that the proper model object can be instantiated.

		if format == self.PYTHON:
			if modelclass is None:
				raise ValueError('Python format requires the modelclass to be provided')
			obj = modelclass(**data)
			return obj
		elif format in [self.JSON, self.XML]:
			return serializers.deserialize(format, data)
		else:
			raise ValueError('Unsupported deserialization format: {}'.format(format))

	# ----------------------------------------------------
	#-== @method
	def validate(self, modelclass, data):
		#-== Convenience method that will instantiate
		# the /data into the /modelclass and then validate it.

		obj = self.deserialize(data, PYTHON, modelclass)
		validate_model_obj(obj)

	# ----------------------------------------------------
	#-== @method
	def validate_model_obj(self, obj):
		#-== Convenience method that runs the /obj.full_clean() validation
		# and logs any errors to both the logs and the HTML template context.

		try:
			obj.full_clean()
		except ValidationError as exc:
			self.log_error(exc.message, exception=exc)



##########################################
#-== @class
class CreateUser(BaseView):
	#-== Form to create or edit a user.
	# This view is used as the parent class for the /EditUser view.

	app_permission = Apps.ADMIN
	page_title = 'Create User'
	back_url = reverse_lazy('core:admin-home')
	PLACEHOLDER_PASSWORD = '**********'

	# ----------------------------------------------------
	#-== @method
	def get_app_permissions(self, user=None):
		#-== Assembles a list of app permissions.
		# @params
		# user: the /CerraxUser to check permissions for.
		#       If no /user is provided, the default permissions
		#       (as defined in the /Apps class)

		app_perms = []
		for app in Apps.list:
			new_entry = deepcopy(app)
			if user:
				new_entry['enabled'] = user.has_app_permission(app['app'])
			app_perms.append(new_entry)
		return app_perms

	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Display the /core\/user-form.html template,
	# populating with context data, if available.

	def get(self, request, pk=None, *args, **kwargs):
		self.context['username'] = ''
		self.context['password'] = ''
		self.context['first_name'] = ''
		self.context['last_name'] = ''
		self.context['app_permissions'] = self.get_app_permissions()
		if pk:
			user = CerraxUser.objects.get(id=pk)
			self.context['userid'] = user.id
			self.context['username'] = user.username
			self.context['password'] = self.PLACEHOLDER_PASSWORD
			self.context['first_name'] = user.first_name
			self.context['last_name'] = user.last_name
			self.context['app_permissions'] = self.get_app_permissions(user)

		return render(request, 'core/user-form.html', self.context)

	# ----------------------------------------------------
	#-== @method
	# POST
	#-== Reads data from the form and creates/edits a user.
	# @params
	# username: the username of the user *(required for new user)*
	# password: the password of the user *(required for new user)*
	# first_name: the primary name to display for the user *(required for new user)*
	# last_name: the surname to display for the user *(required for new user)*
	# <app ID>: if this parameter is supplied, regardless of value,
	#			the app permission will be granted to the user.
	#			If this parameter is not provided,
	#			the user will lose their permission to access the app.

	#-== If a /pk is provided, this will edit that user.
	# Otherwise, a new user will be created.
	# Successful execution redirects to /AdminHome view.


	def post(self, request, pk=None, *args, **kwargs):
		username = request.POST.get('username', None)
		password = request.POST.get('password', self.PLACEHOLDER_PASSWORD)
		first_name = request.POST.get('first_name', None)
		last_name = request.POST.get('last_name', None)
		app_perms = self.get_app_permissions()

		for app in app_perms:
			app['enabled'] = request.POST.get(app['app'], False)

		self.context['userid'] = pk
		self.context['username'] = username
		self.context['password'] = password
		self.context['first_name'] = first_name
		self.context['last_name'] = last_name
		self.context['app_permissions'] = app_perms



		if not username:
			self.log_error('Username cannot be blank.')
		if not password:
			self.log_error('Password cannot be blank.')
		if not first_name:
			self.log_error('First Name cannot be blank.')
		if not last_name:
			self.log_error('Last Name cannot be blank.')

		if self.context['errors']:
			return render(request, 'core/user-form.html', self.context)

		if pk:
			try:
				user = CerraxUser.objects.get(id=pk)
			except Exception as exc:
				self.log_error('Could not find user with ID {}'.format(pk), exception=exc)
				return render(request, 'core/user-form.html', self.context)
		else:
			user = CerraxUser()

		# save user information
		try:
			user.username = username
			user.first_name = first_name
			user.last_name = last_name
			if password != self.PLACEHOLDER_PASSWORD:
				user.password = make_password(password)
			user.save()
		except ValidationError as exc:
			self.log_error(exc, exception=exc)
			return render(request, 'core/user-form.html', self.context)
		except Exception as exc:
			self.log_error('An error occurred while saving the user', exception=exc)
			return render(request, 'core/user-form.html', self.context)

		# save app permissions
		try:
			for app in app_perms:
				if app['enabled']:
					AppPermissions.objects.get_or_create(user=user, app=app['app'])
				else:
					try:
						AppPermissions.objects.get(user=user, app=app['app']).delete()
					except ObjectDoesNotExist:
						# can't delete that which does not exist!
						pass
		except Exception as exc:
			self.log_error('An error occurred while saving permissions', exception=exc)
			return render(request, 'core/user-form.html', self.context)

		return self.redirect('core:admin-home')



##########################################
#-== @class
class EditUser(CreateUser):
	#-== Form to edit or delete a user.
	# This form is derived from the /CreateUser view.
	# See that entry for more details.

	app_permission = Apps.ADMIN
	page_title = 'Edit User'



##########################################
#-== @class
class DeleteUser(BaseView):
	#-== Confirmation screen for deleting a user.

	app_permission = Apps.ADMIN
	page_title = 'Delete User'
	back_url = reverse_lazy('core:admin-home')

	# ----------------------------------------------------
	#-== @method
	# GET
	# -== Diplays the user identified by the /pk
	# and asks the user to confirm the deletion.

	def get(self, request, pk=None, *args, **kwargs):
		user = CerraxUser.objects.get(id=pk)
		self.context['user'] = user
		self.context['back_url'] = reverse_lazy('core:edit-user', args=[pk])
		return render(request, 'core/delete-user.html', self.context)

	# ----------------------------------------------------
	#-== @method
	# POST
	#-== Deletes the user indicated by the /pk
	# and the redirectes to the /AdminHome view.

	def post(self, request, pk=None, *args, **kwargs):
		CerraxUser.objects.get(id=pk).delete()
		return self.redirect('core:admin-home')







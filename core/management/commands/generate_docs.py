
import os
from django.core.management.base import BaseCommand
from django.conf import settings

from candk.candk import CorndogWithKetchup, SINGLE_FILE
from candk.ketchup import Ketchup

#-== @h1
# Core Management Commands
#-== /core.management.commands
#________________________________________

#-== @method
# manage generate_docs

#-== Runs the Corndog with Ketchup modules and
# collects all code documentation into a single /DOCUMENTATION.html file.

class Command(BaseCommand):
    help = 'Generates documentation using Corndog with Ketchup.'

    def handle(self, *args, **options):
        # code documentation
        cd = CorndogWithKetchup(
            search_subs=True,
            output_ext = '.kp',
            output = SINGLE_FILE,
            output_name= 'DOCUMENTATION',
            newline_headers = False,
            collapse_h1 = True,
        )
        cd.begin()
        # website documentation
        docs_dir = os.path.join(settings.BASE_DIR,
                                'website', 'templates',
                                'website', 'pages')
        docs = [
            ('candk.kp', 'candk'),
            ('cx-cloud.kp', 'cx-cloud'),
            ('cxdl.kp', 'cxdl'),
            ('gizeel.kp', 'gizeel'),
            ('projectcx.kp', 'projectcx'),
            ('starfaux.kp', 'starfaux'),
            ('texttool.kp', 'texttool'),
        ]
        for file, outname in docs:
            filepath = os.path.join(docs_dir, file)
            kp = Ketchup(filepath, output_name=outname, output_dir=docs_dir)
            kp.begin()
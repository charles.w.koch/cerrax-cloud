
from django.contrib.auth.models import AbstractUser
from django.db import models

#-== @h1
# Core Models
#-== /core.models.py
#________________________________________


##########################################
#-== @class
class CoreModel(models.Model):
	#-== Abstract base model which tracks
	# when the object was created and last modified.

	# -== *Model Fields:*
	# @deflist
	# created: datetime that the model was added to the database
	# updated: datetime that the most recent change was made to the object

	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	class Meta:
		abstract = True



##########################################
#-== @class
class Apps:
	#-== Unified list of available apps for use throughout the server code.
	# @attributes
	# ADMIN: administrative app to manage users and permissions
	# LISTER: list-making app
	# DOCKET: task tracking app
	# ARKHAM: game

	LISTER = 'lister'
	DOCKET = 'docket'
	#HEALTH = 'health'
	ARKHAM = 'arkham'

	ADMIN = 'admin' # NOTE: admin should always be at the bottom of the list!

	list = [
		{ 'display': 'Lister', 'app': LISTER, 'enabled': True, 'url': 'lister:home' },
		{ 'display': 'Docket', 'app': DOCKET, 'enabled': True, 'url': 'docket:home' },
		#{ 'display': 'Health', 'app': HEALTH, 'enabled': True, 'url': 'health:home' },
		#{ 'display': 'Game Title', 'app': ARKHAM, 'enabled': True, 'url': 'arkham:home' },
		
		# NOTE: admin should always be at the bottom of the list!
		{ 'display': 'Admin', 'app': ADMIN, 'enabled': False, 'url': 'core:admin-home' },
	]

	# ----------------------------------------------------
	#-== @method
	@classmethod
	def perms(cls):
		#-== Provides a list of apps for the
		# /AppPermissions model's /choices parameter

		perms = []
		for app in cls.list:
			perms.append((app['app'], app['display']))
		return perms



##########################################
#-== @class
class CerraxUser(AbstractUser):
	#-== Default Django /User model with additional
	# fields and methods for use with Cerrax Cloud.

	#-== *Model Fields:*
	# - _(see Django /User model: !https://docs.djangoproject.com/en/3.2/ref/contrib/auth/ )_
	
	# ----------------------------------------------------
	#-== @method
	def has_app_permission(self, perm):
		#-== Checks if the user has permission to access an app.
		# @params
		# perm: a string of the app name, typically pulled from the /Apps class

		if self.is_superuser:
			return True
		return self.apppermissions_set.filter(app=perm).exists()

	def __str__(self):
		return self.username



##########################################
#-== @class
class AppPermissions(CoreModel):
	#-== Many-to-many relationship for users and
	# the apps they have permission to use.

	#-== *Model Fields:*
	# @deflist
	# app: the app name the permission grants (derived from the /Apps class)
	# user: the /CerraxUser model which the permission belongs to

	app = models.CharField(max_length=50, choices=Apps.perms())
	user = models.ForeignKey(CerraxUser, on_delete=models.CASCADE)

	def __str__(self):
		return '{} - {}'.format(self.user, self.app)


##########################################
#-== @class
class ErrorMessage(CoreModel):
	#-== One-to-many relationship for users and
	# any unread error messages they have

	#-== *Model Fields:*
	# @deflist
	# message: the message to display
	# user: the /CerraxUser model which the permission belongs to

	message = models.CharField(max_length=1000, default="Generic server error")
	user = models.ForeignKey(CerraxUser, on_delete=models.CASCADE)

	def __str__(self):
		return '{} - {}'.format(self.user, self.message)



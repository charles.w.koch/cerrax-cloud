
#-== @h1
# Core Utils
#-== /core.utils.py
#________________________________________

##########################################
#-== @class
class Sort:
	#-== Data structure for sorting options.
	# @attributes
	# fieldname: the name of the model field to sort by
	# button_display: the text to display on the sorting button
	# reversible: boolean indicating if the sort can be toggled ascending/descending

	def __init__(self, fieldname, button_display, reversible=True):
		self.fieldname = fieldname
		self.button_display = button_display
		self.reversible = reversible

	# ----------------------------------------------------
	#-== @method
	def as_choice_tuple(self, reversed=False):
		#-== Returns the sort as a tuple for use with
		# Django's built-in /choices attribute for /CharField .
		# @params
		# reversed: indicates if the sorting should be
		#			ascending ( /False ) or descending ( /True )

		name = self.fieldname
		display = self.button_display
		if reversed:
			if self.fieldname[0] == '-':
				name = self.fieldname[1:]
			else:
				name = '-'+self.fieldname
		return (name, display)



##########################################
#-== @class
class Sorting:
	#-== A class for storing sort options for a model.
	# @attributes
	# DATA: a list of /Sort objects which can be applied to a model

	DATA = []

	# ----------------------------------------------------
	#-== @method
	@classmethod
	def choices(cls):
		#-== Compiles the /Sort objects in /DATA as a tuple of tuples
		# for use with Django's built-in /choices attribute for /CharField .

		choices = []
		for sort in cls.DATA:
			choices.append(sort.as_choice_tuple())
			if sort.reversible:
				choices.append(sort.as_choice_tuple(reversed=True))
		return tuple(choices)

	# ----------------------------------------------------
	#-== @method
	@classmethod
	def button_bar_display(cls):
		#-== Compiles the /Sort objects in /DATA as a list of options
		# for use with the /button_bar_left attribute for /BaseView .

		button_bar = []
		for sort in cls.DATA:
			button_bar.append({'display': sort.button_display, 'url': '?sort='+sort.fieldname })
		return button_bar


##########################################
#-== @class
class Color:
	#-== Utility methods for handling color

	# ----------------------------------------------------
	#-== @method
	@classmethod
	def fore_color(self, bg_color='#ffffff', light='#000000', dark='#ffffff'):
		#-== Returns a color hex string based on a background color.
		# This is useful for ensuring that text and icons are
		# readable regardless of the background color.
		# @params
		# bg_color: the background color hex string to analyze
		# light: a hex value indicating the color to return
		#        if the background is deemed "light"
		# dark: a hex value indicating the color to return
		#        if the background is deemed "dark"

		fore_color = light
		r = int(bg_color[1:3], 16)
		g = int(bg_color[3:5], 16)
		b = int(bg_color[5:], 16)
		luma = 0.2126 * r + 0.7152 * g + 0.0722 * b
		if luma < 110:
			fore_color = dark
		return fore_color
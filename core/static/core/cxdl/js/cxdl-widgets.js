
// CXDL Widgets
//---------------------------------
// jQuery/JS code that runs the various elements of the app

var dragging = false;

// animation and other variables for the navbar
var navbar = ".navbar";
var navbar_toggle = ".navbar-toggle";
var navbar_duration = 200;
var navbar_easing = "swing"
var navbar_active = "active"
var nav_width;  // this gets set on $(document).ready()

// variables for checkbox
var checkbox = ".checkbox-aura";
var checkbox_div_suffix = "-div";
var checkbox_suffix = "-input";
var checkbox_active = "checked";

// variables for dialog
var dialog = ".dialog";
var dialog_toggle = ".dialog-toggle";
var dialog_duration = 250;
var dialog_easing = "swing"
var dialog_active = "active"
	
// varibles for toggle button
var t_button = ".toggle-btn";
var t_button_active = "active";

// variables for toast
var toast = ".toast";
var toast_duration = 400;
var toast_easing = "swing";
var toast_default_stayopen = 3;
var toast_default_dir = "bottom";
var toast_timeout; // this gets set when the toast opens

// variables for collapse
var collapse = ".collapse";
var collapse_toggle = ".collapse-toggle";
var collapse_content = ".collapse-content";
var collapse_duration = 200;
var collapse_easing = "swing";
var collapse_fade_duration = 200;
var collapse_fade_easing = "swing";
var collapse_active = "expanded";
var collapse_elements = {};

// variables for tabs
var tabs = ".tab";
var tab_bar = ".tab-bar";
var tab_content = ".tab-content";
var tab_active = "active";
var tab_content_duration = 200;
var tab_content_easing = "swing";

$(window).on('load resize', function () {
	if($(window).width() < 570){
		$(".hide-small").hide();
		$(".truncate-small").addClass("truncate-text");
	}
	else{
		$(".hide-small").show();
		$(".truncate-small").removeClass("truncate-text");
	}
});

// prepare the document and all elements with interactivity
$(document).ready( function () {

	prepare_document();
	prepare_navbar();
	prepare_checkboxes();
	prepare_dialogs();
	prepare_toggle_buttons();
	prepare_toasts();
	prepare_collapses();
	prepare_tabs();
	prepare_buttons();
	// all widgets prepared, document is ready for interactivity
	$(document).trigger("widgetsready");
});

function prepare_document() {
	$("*").on("touchstart", function() {dragging = false; });
	$("*").on("touchmove", function() {dragging = true; });
}

function prepare_buttons() {
	$("a").on("touchend", function() { 
		location.href = $(this).closest("a").attr("href");
	});
	$("button").on("touchend", function() { $(this).click(); });
}

function prepare_tabs() {
	$(tabs).on("mouseup touchend",function(evt) {
		// we do not want to fire this if the user is scrolling
		if(dragging) { return; }
		
		switch_tab($(this));
		
		dragging = false;
	});
	$(tabs).each( function(index, element) {
		if(!$(element).hasClass(tab_active)) {
			var page = $("#" + $(element).data("tab"));
			page.css("opacity", "0");
			page.hide();
		}
	});
}

function switch_tab(thetab) {
	var tab_group = thetab.data("group")
	if(thetab.hasClass(tab_active)) {
		return;
	}
	var current_tab = $(tabs+"."+tab_active).filter(function() { 
		return $(this).data("group") == tab_group;
	});
	var current_page = $("#" + current_tab.data("tab"));
	var page = $("#" + thetab.data("tab"));
	current_tab.removeClass(tab_active);
	thetab.addClass(tab_active);
	current_page.animate({
		opacity: 0
	},
	tab_content_duration,
	tab_content_easing,
	function() {
		current_page.hide();
		page.show();
		page.animate({
			opacity: 1
		},
		tab_content_duration,
		tab_content_easing);
	});
	
}

function prepare_collapses() {
	$(collapse_toggle).on("mouseup touchend",function(evt) {
		preventDefaultCustom(evt);
		// we do not want to fire this if the user is scrolling
		if(dragging) { return; }
		
		colltoggle = $(this);
		var coll_id = $(this).data("collapse")
		// close all collapses in the same group as this one
		if($(this).data("group") !== undefined) {
			var group = $(this).data("group");
			var coll_group = $(collapse_toggle).filter(function() {
				return $(this).data("group") == group;
			});
			coll_group.each( function() {
				var id = $(this).data("collapse")
				if (id != coll_id) {
					var thecollapse = $("#"+id);
					$(this).removeClass(collapse_active);
					close_collapse(thecollapse);
				}
			});
			coll_group.promise().done(function() {
				toggle_collapse(colltoggle);
			});
		}
		else {
			toggle_collapse(colltoggle);
		}
		evt.stopPropagation();
		dragging = false;
	});
	get_collapse_sizes();
}

function get_collapse_sizes() {
	$(collapse).each( function(index, element) {
		collapse_elements[$(element).attr("id")] = {"height": $(element).outerHeight(), "width": $(element).outerWidth()};
		if(!$(element).hasClass(collapse_active)) {
			$(element).children(collapse_content).css("opacity", "0");
			$(element).children(collapse_content).hide();
			$(element).css("height", "0");
			$(element).hide();
		}
	});
}

function toggle_collapse(colltoggle) {
	var id = colltoggle.data("collapse");
	var thecollapse = $("#"+id);
	if(thecollapse.hasClass(collapse_active)) {
		close_collapse(thecollapse);
		colltoggle.removeClass(collapse_active);
	}
	else {
		colltoggle.addClass(collapse_active);
		open_collapse(thecollapse);
	}
}

function close_collapse(thecollapse) {
	thecollapse.css("height", thecollapse.outerHeight());
	if(thecollapse.hasClass(collapse_active)) {
		collapse_elements[thecollapse.attr("id")] = {"height": thecollapse.outerHeight(), "width": thecollapse.outerWidth()};
	}
	else {
		return;
	}
	var content = thecollapse.children(collapse_content);
	content.animate({
		opacity: 0
	},
	collapse_fade_duration,
	collapse_fade_easing
	);

	content.hide();
	thecollapse.animate({
		height: 0
	},
	collapse_duration,
	collapse_easing,
	function() {
		$(this).removeClass(collapse_active);
		thecollapse.hide();
	});
}

function open_collapse(thecollapse) {
	var content = thecollapse.children(collapse_content);
	thecollapse.show();
	thecollapse.animate({
		height: collapse_elements[thecollapse.attr("id")]["height"]
	},
	collapse_duration,
	collapse_easing
	);
	
	content.show();
	content.animate({
		opacity: 1
	},
	collapse_fade_duration,
	collapse_fade_easing,
	function() {
		thecollapse.addClass(collapse_active);
		thecollapse.css("height", "");
	});
}


function prepare_toasts() {
	$(".toast").each( function(index, element) {
		start_toast($(element));
	});
}

function start_toast(thetoast) {
	var direction = thetoast.data("direction");
	//thetoast.css("opacity", "0");
	if(direction == "right") {
		thetoast.css("left", $(window).width().toString()+"px");
	}
	else if(direction == "left") {
		thetoast.css("left", "-"+thetoast.outerWidth(true).toString()+"px");
	}
	else if(direction == "top") {
		thetoast.css("top", "-"+thetoast.outerHeight(true).toString()+"px");
	}
	else {
		thetoast.css("top", $(window).height().toString()+"px");
	}
	thetoast.hide();
}

function end_toast(thetoast) {
	thetoast.animate({
		opacity: 0},
		toast_duration,
		toast_easing,
		function() {
			start_toast(thetoast);
		});
}

function open_toast(id) {
	var thetoast = $("#"+id);
	var stayopen = thetoast.data("stayopen");
	if(stayopen === undefined) {
		stayopen = toast_default_stayopen
	}
	start_toast(thetoast);
	thetoast.show();
	thetoast.animate({
		top: 0,
		left: 0,
		opacity: 1},
		toast_duration,
		toast_easing,
		function() {
			if(stayopen > 0) {
				clearTimeout(toast_timeout);
				toast_timeout = setTimeout(function() { end_toast(thetoast); }, stayopen*1000);
			}
		});
}

function close_toast(id) {
	var thetoast = $("#"+id);
	end_toast(thetoast);
}

function update_badge(id, value) {
	var badge = $("#"+id);
	if(value == 0){
		badge.hide();
	}
	else {
		badge.text(value);
		badge.show();
	}
}

function prepare_toggle_buttons() {
	$(t_button).on("mouseup touchend", function(evt) {
		preventDefaultCustom(evt);
		// we do not want to fire this if the user is scrolling
		if(dragging) { return; }
		
		var id = $(this).attr("id");
		toggle_button(id);
		evt.stopPropagation();
		dragging = false;
	});
	$(document).on("mouseup touchend", function(evt) {
		// if the event makes it all the way to the document
		// then the click was not in the toggle
		preventDefaultCustom(evt);
		// we do not want to fire this if the user is scrolling
		if(dragging) { return; }
		
		$(t_button).removeClass(t_button_active);
		dragging = false;
	});
}

function toggle_button(id) {
	var toggle = $("#"+id);
	if(toggle.hasClass(t_button_active)) {
		toggle.removeClass(t_button_active);
	}
	else {
		toggle.addClass(t_button_active);
	}
}

function prepare_dialogs() {
	$(dialog_toggle).on("mouseup touchend",function(evt) {
		preventDefaultCustom(evt);
		// we do not want to fire this if the user is scrolling
		if(dragging) { return; }
		
		var id = $(this).data("dialog");
		var position = $(this).data("position");
		toggle_dialog(id, $(this), position);
		// change button state if its marked as a toggle button
		if($(this).hasClass(t_button)){
			toggle_button($(this).attr("id"));
		}
		// close all dialogs in the same group as this one
		if($(this).data("group") !== undefined) {
			var group = $(this).data("group");
			var dialog_group = $(dialog_toggle).filter(function() {
				return $(this).data("group") == group;
			});
			dialog_group.each( function() {
				if ($(this).data("dialog") != id) {
					close_dialog($(this).data("dialog"));
				}
			});
		}
		evt.stopPropagation();
		dragging = false;
	});
	$(dialog).on("mouseup touchend",function(evt) {
		// we do not want to fire this if the user is scrolling
		if(dragging) { return; }
		// stop this from signalling the document
		evt.stopPropagation();
		dragging = false;
	});
	$(document).on("mouseup touchend", function(evt) {
		preventDefaultCustom(evt);
		// we do not want to fire this if the user is scrolling
		if(dragging) { return; }
		// if the event makes it all the way to the document
		// then the click was not in the dialog
		close_dialogs();
		dragging = false;
	});
}

function toggle_dialog(id, element, position) {
	if($("#"+id).hasClass(dialog_active)) {
		close_dialog(id);
	}
	else {
		open_dialog(id, element, position);
	}
}

function open_dialog(id, target, pos) {
	var dialog_box = $("#"+id);
	var coords = get_dialog_coords(id, target, pos);
	dialog_box.css(coords);
	dialog_box.show();
	// (could animate this if necessary)
	dialog_box.addClass(dialog_active);
}

function get_dialog_coords(id, target, pos) {
	var dialog_box = $("#"+id);
	var y;
	var x;
	var target_pos = $(target).offset();
	var target_height = $(target).outerHeight();
	var target_width = $(target).outerWidth();
	var dialog_width = dialog_box.outerWidth();
	var dialog_height = dialog_box.outerHeight();
	// check if it has a data-position set
	if(pos == "bottom-left") {
		y = target_pos.top + target_height;
		x = target_pos.left - (dialog_width - target_width);
	}
	else if(pos == "bottom-right") {
		y = target_pos.top + target_height;
		x = target_pos.left;
	}
	else if(pos == "bottom-center") {
		y = target_pos.top + target_height;
		x = target_pos.left + (target_width/2.0) - (dialog_width/2.0);
	}
	else if(pos == "top-left") {
		y = target_pos.top - dialog_height;
		x = target_pos.left - (dialog_width - target_width);
	}
	else if(pos == "top-right") {
		y = target_pos.top - dialog_height;
		x = target_pos.left;
	}
	else if(pos == "top-center") {
		y = target_pos.top - dialog_height;
		x = target_pos.left + (target_width/2.0) - (dialog_width/2.0);
	}
	else if(pos == "center") {
		y = target_pos.top + (target_height/2.0) - (dialog_height/2.0);
		x = target_pos.left + (target_width/2.0) - (dialog_width/2.0);
	}
	else {
		y = -(dialog_height/2.0)+($(window).height()/2.0);
		x = -(dialog_width/2.0)+($(window).width()/2.0);
	}
	return { "top": y.toString()+"px", "left": x.toString()+"px"};
}

function close_dialog(id) {
	var dialog_box = $("#"+id);
	dialog_box.hide();
	// (could animate this if necessary)
	dialog_box.removeClass(dialog_active);
}

function close_dialogs() {
	$(dialog).each(function (index, element) {
		var id = $(element).attr("id");
		close_dialog(id);
	});
}

function prepare_checkboxes() {
	$(checkbox).each( function () {
		var id = $(this).attr("id");
		var input = $("#" + id + checkbox_suffix);
		var div = $("#" + id + checkbox_div_suffix);
		if(input.prop(checkbox_active)) {
			div.addClass(checkbox_active);
		}
		if(!input.prop("disabled")){
			$(this).on("mouseup touchend",function(evt) {
				preventDefaultCustom(evt);
				// we do not want to fire this if the user is scrolling
				if(dragging) { return; }
				
				toggle_checkbox(id);
				dragging = false;
			});
		}
	});
}

function toggle_checkbox(id) {
	var input = $("#" + id + checkbox_suffix);
	var div = $("#" + id + checkbox_div_suffix);
	if(input.prop(checkbox_active)){
		input.prop(checkbox_active, false);
		div.removeClass(checkbox_active);
	}
	else {
		input.prop(checkbox_active, true);
		div.addClass(checkbox_active);
	}
}

function prepare_navbar() {
	$(navbar).addClass("invisible");
	nav_width = -$(navbar).outerWidth(true);
	if(nav_width === undefined) {
		return;
	}
	nav_width = nav_width.toString();
	$(navbar).css("left", nav_width);
	// add event listeners related to the navbar
	$(navbar).on("mouseup touchend",function(evt) {
		// we do not want to fire this if the user is scrolling
		if(dragging) { return; }
		// stop this from signalling the document
		evt.stopPropagation();
		dragging = false;
	});
	$(navbar_toggle).on("mouseup touchend",function(evt) {
		preventDefaultCustom(evt);
		// we do not want to fire this if the user is scrolling
		if(dragging) { return; }
		
		// toggle the navbar
		toggle_navbar();
		// stop this from signalling the document
		evt.stopPropagation();
		dragging = false;
	});
	$(document).on("mouseup touchend", function(evt) {
		preventDefaultCustom(evt);
		// we do not want to fire this if the user is scrolling
		if(dragging) { return; }
		// if the event makes it all the way to the document
		// then the click was not in the navbar or the toggle
		close_navbar();
		dragging = false;
	});
}

function toggle_navbar() {
	if($(navbar).hasClass(navbar_active)) {
		close_navbar();
	}
	else {
		open_navbar();
	}
	
}

function open_navbar() {
	$(navbar).removeClass("invisible");
	$(navbar).animate({
		left: 0
	},
	navbar_duration,
	navbar_easing,
	function() {
		$(this).addClass(navbar_active)
	});
}

function close_navbar() {
	$(navbar).animate({
		left: nav_width
	},
	navbar_duration,
	navbar_easing,
	function() {
		$(this).removeClass(navbar_active)
		$(navbar).addClass("invisible");
	});
}


function preventDefaultCustom(evt) {
	if (evt.target.tagName != 'SELECT' && evt.target.tagName != 'INPUT') {
        evt.preventDefault();
    }
}
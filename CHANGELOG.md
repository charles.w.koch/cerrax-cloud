
# Changelog

All notable changes to this project will be documented in this file.


# 2.1.0

## New Features

- **Website:** Added the Gizeel-scale temperature gauge
- **Lister:** A superuser now has full permissions to edit or delete any list that has been shared with them

## Fixes and Maintenance

- **Website:** Fixed server error occurring in the StarFaux documentation


# 2.0.2

## Fixes and Maintenance

- Added files and changes to facilitate transition to in-house server
- Added a favicon


# 2.0.1

## Fixes and Maintenance

- **Docket:** Fixed an issue on desktop where the purge setting textbox was too small to see the full value
- **Docket:** Fixed 500 error being thrown on Create New Item
- **Website:** Added missing documentation for TextTool 2
- **Website:** Fixed incorrect thumbnail images on Software page
- **Website:** Fixed HTML tags showing up on Software cards
- **Website:** Updated resume and website content
- **Website:** Changed software cards to include Git repo links


# 2.0.0-hotfix-1

## Fixes and Maintenance

- Replaced `ConcurrentLogHandler` package with `concurrent-log-handler`
  - Django 4.2 requires SQLite 3.21, which was not available in PythonAnywhere's default Python 3.8 system image
  - Changed the production system image to Python 3.10
  - `ConcurrentLogHandler` is no longer maintained, and was not able to be installed in Python 3.10
  - `concurrent-log-handler` is a fork of that repo that is more up-to-date and currently maintained


# 2.0.0

 Cerrax.com is now a part of Cerrax Cloud! Or... is Cerrax Cloud now a part of Cerrax.com...?

Not quite yet. This release integrates the current Cerrax.com website into the Cerrax Cloud web server. However, additional work is needed before the domain ```http://www.cerrax.com``` is pointed to the Cerrax Cloud (which currently resides on PythonAnywhere).

## New Features

- **Cerrax.com** rendering and navigation is now handled within the Django server.
  - This unlocks a lot of future potential by leveraging Python to extend website capabilities.
- A new app (working name "Arkham") has been added to the Cerrax Cloud URLs, however, the app is **not** active.
  - Arkham URLs will give a 404 or 500 errors, as they are not yet ready.

## Deprecations

- Lister app URLs have been changed to permanently redirect to their new ```/cloud/lister``` URLs

## Breaking Changes

- All cloud URLs have been modified with a ```/cloud``` prefix
  - A permanent redirect (HTTP 301) is in place for the Lister app, which will be removed at a later date
  - All other apps (Admin, Core, and Docket) do not have redirects, as they are not widely used

## Fixes and Maintenance

- Django has been upgraded to 4.2.13


# 1.1.2

## Fixes and Maintenance

- **Docket:** Fixed an issue where "old" items were not getting colored correctly according to their color setting.


# 1.1.1-hotifx-1

## Fixes and Maintenance

- **Docket:** Fixed an issue where filters were not being properly applied


# 1.1.1

## New Features

- **Logging:** Cerrax Cloud now stores Django logs as well as Cerrax Cloud specific logs for debugging and troubleshooting.

## Fixes and Maintenance

- **Docket:** Fixed an error which occurred when attempting to filter by the "Closed" status
- **Docket:** Fixed incorrect date display in all screens
- **All Apps:** Fixed issues with scrolling and resizing on mobile devices
- Formalized the development environment to make future development and contribution easier
  - Added `requirements.txt` file to ensure proper dependencies
  - Split `settings.py` into "base_settings" and "settings" to allow for settings to differ between dev and prod
  - Updated `README` file with much more detail and complete deployment process


# 1.1.0

## New Features

- **New App - Docket:** A high-level project tracking app
  - Add an item to your Docket that will track the Priority, Status, Category, and many other details
  - Sort and filter items by name, priority, status, category, latest update, or other data
  - Color-coded date field indicates when items are stagnant or about to be automatically deleted
  - Keep track of incremental updates to the item as it progresses
  - Create, edit, and delete your own categories to sort and filter items
  - Customizable colors for Status, Priority, and Update fields


# 1.0.1-hotfix-1

## Fixes and Maintenance

- Hotfixes based on de-synced code on web server and git repo


# 1.0.1

## Fixes and Maintenance

- Error messages are now preserved and displayed upon redirect


# 1.0.0

Welcome to Cerrax Cloud! A centralized location for all of my various apps and experiments to live.

## New Features

- **User Logins:** Each user has their own settings, data, and permissions for the apps they access. Cerrax Cloud requires a username in order to access it.
- **User Admin:** Special administration app for handling users.
  - Create, modify, and delete users 
  - Set permissions for individual Cerrax Cloud apps (including the Admin app itself)
- **New App - Lister:** A list-making app which allows you to organize checklists and share them with other Cerrax Cloud users.
  - Create, edit, and remove lists
  - Share lists with other Cerrax Cloud users
  - Control who can view, edit, or delete a shared list
  - Sort and filter list items
  - Automatic data purge of marked items

# Generated by Django 3.2 on 2024-02-07 21:23

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='DocketCategory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=30)),
                ('hide_items', models.BooleanField(default=False)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DocketItem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(max_length=100)),
                ('priority', models.IntegerField(default=2)),
                ('status', models.IntegerField(default=1)),
                ('contact', models.CharField(blank=True, default='', max_length=50)),
                ('last_update', models.DateField()),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='docket.docketcategory')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DocketSettings',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('update_new_color', models.CharField(default='ffffff', max_length=6)),
                ('update_aging_days', models.IntegerField(default=2)),
                ('update_aging_color', models.CharField(default='fbe8ac', max_length=6)),
                ('update_old_days', models.IntegerField(default=5)),
                ('update_old_color', models.CharField(default='ff2600', max_length=6)),
                ('update_purge_warning', models.IntegerField(default=10)),
                ('update_purge_color', models.CharField(default='646464', max_length=6)),
                ('purge_days', models.IntegerField(default=240)),
                ('status_open_color', models.CharField(default='ffffff', max_length=6)),
                ('status_working_color', models.CharField(default='cadbfa', max_length=6)),
                ('status_onhold_color', models.CharField(default='e06664', max_length=6)),
                ('status_closed_color', models.CharField(default='b8e1cc', max_length=6)),
                ('priority_low_color', models.CharField(default='ffffff', max_length=6)),
                ('priority_medium_color', models.CharField(default='fbe8ac', max_length=6)),
                ('priority_high_color', models.CharField(default='e06664', max_length=6)),
                ('sorting', models.CharField(choices=[('title', 'Title'), ('-title', 'Title'), ('category', 'Category'), ('-category', 'Category'), ('priority', 'Priority'), ('-priority', 'Priority'), ('status', 'Status'), ('-status', 'Status'), ('created', 'Created'), ('-created', 'Created'), ('updated', 'Updated'), ('-updated', 'Updated')], default='priority', max_length=30)),
                ('filter_hide_status', models.CharField(blank=True, default='', max_length=20)),
                ('filter_hide_priority', models.CharField(blank=True, default='', max_length=20)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DocketItemUpdate',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('text', models.CharField(max_length=250)),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='docket.docketitem')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]

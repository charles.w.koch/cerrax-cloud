
import logging
import datetime

from django.utils import timezone
from django.views import View
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.utils.html import escape
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.shortcuts import render
from django.urls import reverse_lazy
from django.db.models import Q
from django.db.models.functions import Lower

from core.models import Apps, CerraxUser
from core.views import BaseView, CrudMixin
from core.utils import Color
from docket.models import DocketSorting, DocketStatus, DocketPriority, DocketCategory, DocketSettings, DocketItem, DocketItemUpdate


#-== @h1
# Docket Views
#-== /docket.views.py
#________________________________________


##########################################
#-== @class
class DocketBase(BaseView):
	#-== Sets the /app_permission for all derived views as /Apps.DOCKET .

	app_permission = Apps.DOCKET

	# ----------------------------------------------------
	#-== @method
	def initialize(self, request, *args, **kwargs):
		#-== Fetch the settings and categories associated with the request user.
		# This adds 2 additional attributes to the view class.
		# @attributes
		# self.settings: the /DocketSettings object for the user
		# self.categories: the /DocketCategory objects associated with the user

		self.settings = DocketSettings.objects.get_or_create(user=self.user)[0]
		self.categories = DocketCategory.objects.filter(user=self.user)
		if len(self.categories) == 0:
			DocketCategory.objects.create(user=self.user, name='Personal')
			DocketCategory.objects.create(user=self.user, name='Work')
			self.categories = DocketCategory.objects.filter(user=self.user)


##########################################
#-== @class
class DocketHome(DocketBase):
	#-== Shows all of the Docket items available to the user.
	# This list of items is sorted and filtered according to the
	# parameters chosen by the user on the Sort and Filter pages.

	page_title = 'Docket'
	back_url = reverse_lazy('core:landing')
	button_bar_left = [
		{'display': 'Sort', 'url': reverse_lazy('docket:sort') },
		{'display': 'Filter', 'url': reverse_lazy('docket:filter') },
	]
	button_bar_right = [
		{'display': '+ New Item', 'url': reverse_lazy('docket:create-item') },
		{'display': 'Settings', 'url': reverse_lazy('docket:settings') },
	]

	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Display the docket items (sorted and filtered).

	def get(self, request, *args, **kwargs):

		self.purge_old_items()

		exclusions = self.settings.get_exclusions()
		sorting = self.settings.sorting
		items = DocketItem.objects.filter(user=self.user
			).exclude(**exclusions['statuses']
			).exclude(**exclusions['priorities']
			).exclude(**exclusions['categories']
			).order_by(sorting)

		for item in items:
			item.set_display()

		self.context['docketitems'] = items
		return render(request, 'docket/home.html', self.context)

	# ----------------------------------------------------
	#-== @method
	def purge_old_items(self):
		#-== Every time the view is requested, this method checks
		# for old /DocketItem objects and deletes them according
		# to the user's purge settings.

		purge_days = self.settings.purge_days
		if purge_days >= 0:
			purge_threshold = timezone.now() - datetime.timedelta(days=purge_days)
			old_items = DocketItem.objects.filter(user=self.user, last_update__lt=purge_threshold.date())
			num_deleted = old_items.delete()[0]
			if num_deleted > 0:
				self.logger.info('Purged {} items older than {} days'.format(num_deleted, purge_days))

##########################################
#-== @class
class SortPage(DocketBase):
	#-== Allows the user to select from a list of sorting options.

	page_title = 'Sort'
	back_url = reverse_lazy('docket:home')

	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Display the available sorting options.

	def get(self, request, *args, **kwargs):
		direction = 'asc'
		sort = self.settings.sorting
		if sort[0] == '-':
			direction ='desc'
			sort = sort [1:]

		self.context['sort'] = {
			'direction': direction,
			'sort': sort,
		}

		return render(request, 'docket/sort.html', self.context)

	# ----------------------------------------------------
	#-== @method
	# POST
	#-== Update the sorting to the selected sorting option.

	def post(self, request, *args, **kwargs):
		# set the direction of the sort (ascending or descending)
		directionstr = request.POST.get('direction')
		if not directionstr:
			self.log_error('Sort direction is invalid.')
		direction = ''
		if directionstr == 'asc':
			direction = ''
		elif directionstr == 'desc':
			direction = '-'
		else:
			self.log_error('Sort direction is invalid.')

		# get the field to sort by
		sortstr = request.POST.get('sort')
		for sort in DocketSorting.DATA:
			if sortstr == sort.fieldname:
				break
		else:
			self.log_error('Sort field is invalid.')

		# save to the settings object
		self.settings.sorting = direction+sortstr
		try:
			self.settings.save()
		except Exception as exc:
			self.log_error('An error occurred while trying to save sort settings.', exception=exc)

		return self.redirect('docket:home')

##########################################
#-== @class
class FilterPage(DocketBase):
	#-== Allows the user to select from a list of sorting options.

	page_title = 'Filter'
	back_url = reverse_lazy('docket:home')

	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Display the available sorting options.

	def get(self, request, *args, **kwargs):

		self.context['status_open'] = True
		self.context['status_working'] = True
		self.context['status_onhold'] = True
		self.context['status_closed'] = True
		status_excludes = self.settings.get_excluded_statuses()
		if DocketStatus.OPEN in status_excludes:
			self.context['status_open'] = False
		if DocketStatus.WORKING in status_excludes:
			self.context['status_working'] = False
		if DocketStatus.ONHOLD in status_excludes:
			self.context['status_onhold'] = False
		if DocketStatus.CLOSED in status_excludes:
			self.context['status_closed'] = False

		self.context['priority_low'] = True
		self.context['priority_medium'] = True
		self.context['priority_high'] = True
		priority_excludes = self.settings.get_excluded_priorities()
		if DocketPriority.LOW in priority_excludes:
			self.context['priority_low'] = False
		if DocketPriority.MEDIUM in priority_excludes:
			self.context['priority_medium'] = False
		if DocketPriority.HIGH in priority_excludes:
			self.context['priority_high'] = False

		self.context['categories'] = DocketCategory.objects.filter(user=self.user)

		return render(request, 'docket/filter.html', self.context)

	# ----------------------------------------------------
	#-== @method
	# POST
	#-== Update the sorting to the selected sorting option.

	def post(self, request, *args, **kwargs):
		exclude_stats = []
		if not request.POST.get('status_open', False):
			exclude_stats.append(str(DocketStatus.OPEN.value))
		if not request.POST.get('status_working', False):
			exclude_stats.append(str(DocketStatus.WORKING.value))
		if not request.POST.get('status_onhold', False):
			exclude_stats.append(str(DocketStatus.ONHOLD.value))
		if not request.POST.get('status_closed', False):
			exclude_stats.append(str(DocketStatus.CLOSED.value))

		self.settings.filter_hide_status = ','.join(exclude_stats)

		exclude_pris = []
		if not request.POST.get('priority_low', False):
			exclude_pris.append(str(DocketPriority.LOW.value))
		if not request.POST.get('priority_medium', False):
			exclude_pris.append(str(DocketPriority.MEDIUM.value))
		if not request.POST.get('priority_high', False):
			exclude_pris.append(str(DocketPriority.HIGH.value))

		self.settings.filter_hide_priority = ','.join(exclude_pris)

		try:
			self.settings.save()
		except Exception as exc:
			self.log_error('An error occurred while trying to save status and priority filters.', exception=exc)

		categories = DocketCategory.objects.filter(user=self.user)
		for cat in categories:
			cat.hide_items = not bool(request.POST.get('category_{}'.format(cat.id), False))
			try:
				cat.save()
			except Exception as exc:
				self.log_error('An error occurred while trying to save a category filter.', exception=exc)

		return self.redirect('docket:home')

##########################################
#-== @class
class DocketSettingsPage(DocketBase):
	#-== A page to adjust the /DocketSettings and /DocketCategory objects of the user.

	page_title = 'Settings'
	back_url = reverse_lazy('docket:home')

	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Display the user's current settings within the /docket\/settings.html template.

	def get(self, request, *args, **kwargs):
		self.context['settings'] = self.settings
		self.context['categories'] = self.categories

		return render(request, 'docket/settings.html', self.context)

	# ----------------------------------------------------
	#-== @method
	# POST
	#-== Adjust the settings based on the data received from the form.

	def post(self, request, *args, **kwargs):
		print(request.POST)

		self.update_setting(request, 'update_new_color')
		self.update_setting(request, 'update_aging_days')
		self.update_setting(request, 'update_aging_color')
		self.update_setting(request, 'update_old_days')
		self.update_setting(request, 'update_old_color')
		self.update_setting(request, 'update_purge_warning')
		self.update_setting(request, 'update_purge_color')
		self.update_setting(request, 'purge_days')
		self.update_setting(request, 'status_open_color')
		self.update_setting(request, 'status_working_color')
		self.update_setting(request, 'status_onhold_color')
		self.update_setting(request, 'status_closed_color')
		self.update_setting(request, 'priority_low_color')
		self.update_setting(request, 'priority_medium_color')
		self.update_setting(request, 'priority_high_color')

		try:
			self.settings.save()
		except Exception as exc:
			self.log_error('An error occurred while updating settings', exception=exc)
			self.redirect('docket:settings')

		# check if we have a request to delete a category
		delete_cat_id = int(request.POST.get('submit_deletecategory', -1))

		for cat in self.categories:
			if cat.id == delete_cat_id:
				# if we match the delete ID, delete it
				try:
					cat.delete()
				except ProtectedError:
					self.log_error(cat.name + ' - You cannot delete a category that is used by any Docket items.')
			else:
				# if it doesn't match the delete ID, let's update it
				catname = 'category_{}'.format(cat.id)
				self.update_setting(request, catname, obj=cat, objfield='name')
				cat.save()

		# check for a new category trigger and create it
		newcategory_trigger = request.POST.get('submit_newcategory', None)

		if newcategory_trigger:
			newcategory_name = request.POST.get('newcategory', None)
			if not newcategory_name:
				self.log_error('You cannot create a category with an empty name.')
			DocketCategory.objects.create(user=self.user, name=newcategory_name)

		# if we've done any operations to add or delete categories,
		# make sure we add an anchor to the URL, so that the user is taken
		# back to the part of the page they were viewing before the submit.
		url = 'docket:home'
		if delete_cat_id > 0 or newcategory_trigger:
			url = reverse_lazy('docket:settings') + '#categories'

		return self.redirect(url)

	# ----------------------------------------------------
	#-== @method
	def update_setting(self, request, name, obj=None, objfield=None):
		#-== Pulls a value from the /request.POST dictionary and
		# writes the value to an object based on the /name provided.
		# @params
		# request: the request object of the view
		# name: the name of the attribute to read from POST data
		# obj: a Python object the value belongs to.
		#      If no /obj is provided, default is /self.settings
		# objfield: the field name in the object if it is different from /name
		#
		#-== @note
		# If the value pulled from the POST is not truthy
		# (i.e. /None, empty string, empty list, etc.),
		# then the the /obj will not be updated.

		value = request.POST.get(name, None)
		if not obj:
			obj = self.settings
		if not objfield:
			objfield = name
		if value:
			setattr(obj, objfield, value)

##########################################
#-== @class
class CreateItem(DocketBase):
	#-== Form to create or edit a /DocketItem object.
	# This view is used as the parent class for the /EditItem view.

	page_title = 'Create Item'
	back_url = reverse_lazy('docket:home')

	# ----------------------------------------------------
	#-== @method
	def get_item_and_set_context(self, pk=None):
		#-== Retrieve a /DocketItem object and populate the view context.
		# If no /pk is provided, a blank /DocketItem is instantiated.
		# @params
		# pk: The primary key of a /DocketItem object

		item = DocketItem(user=self.user)
		if pk:
			item = DocketItem.objects.get(id=pk)
		else:
			item.created = timezone.now()

		item.set_display()

		self.context['item'] = item
		self.context['extra'] = {
			'history': DocketItemUpdate.objects.filter(item=item).order_by('created'),
		}

		self.context['statuses'] = [
			{ 'id': DocketStatus.OPEN.value, 'text': 'Open' },
			{ 'id': DocketStatus.WORKING.value, 'text': 'Working' },
			{ 'id': DocketStatus.ONHOLD.value, 'text': 'On Hold' },
			{ 'id': DocketStatus.CLOSED.value, 'text': 'Closed' },
		]
		for stat in self.context['statuses']:
			bg_color = self.settings.get_status_color(stat['id'])
			fore_color = Color.fore_color(bg_color)
			stat['bg_color'] = bg_color
			stat['fore_color'] = fore_color

		self.context['priorities'] = [
			{ 'id': DocketPriority.LOW.value, 'text': 'Low' },
			{ 'id': DocketPriority.MEDIUM.value, 'text': 'Medium' },
			{ 'id': DocketPriority.HIGH.value, 'text': 'High' },
		]
		for pri in self.context['priorities']:
			bg_color = self.settings.get_priority_color(pri['id'])
			fore_color = Color.fore_color(bg_color)
			pri['bg_color'] = bg_color
			pri['fore_color'] = fore_color

		self.context['categories'] = DocketCategory.objects.filter(user=self.user)

		return item

	# ----------------------------------------------------
	#-== @method
	def validate_post_data(self, request):
		#-== Validates item data received from POST
		# and returns a tuple of /-(data , is_valid)-/ .
		# @params
		# request: A request with the POST data

		data = {
			'title': request.POST.get('title', None),
			'status': int(request.POST.get('status', -1)),
			'priority': int(request.POST.get('priority', -1)),
			'contact': request.POST.get('contact', None),
			'category': request.POST.get('category', None),
			'newupdate': request.POST.get('newupdate', None),
		}

		data_is_valid = True
		title = data['title']
		if not title:
			data_is_valid = False
			self.log_error('Title cannot be empty.')
		if len(title) > 100:
			data_is_valid = False
			self.log_error('Title cannot be longer than 100 characters.')

		category_id = data['category']
		if not category_id:
			data_is_valid = False
			self.log_error('You must select a category.')
		try:
			category = DocketCategory.objects.get(id=category_id)
			data['category'] = category
		except ObjectDoesNotExist:
			data_is_valid = False
			self.log_error('Invalid category selected.')

		status = data['status']
		if status not in DocketStatus.__members__.values():
			data_is_valid = False
			self.log_error('Invalid status selected.')

		priority = data['priority']
		if priority not in DocketPriority.__members__.values():
			data_is_valid = False
			self.log_error('Invalid priority selected.')

		contact = data['contact']
		if len(contact) > 50:
			data_is_valid = False
			self.log_error('Contact cannot be longer than 50 characters.')

		return data, data_is_valid


	# ----------------------------------------------------
	#-== @method
	# GET
	#-== Display the /docket\/item.html template,
	# populating with context data, if available.

	def get(self, request, pk=None, *args, **kwargs):

		item = self.get_item_and_set_context(pk)
		return render(request, 'docket/item.html', self.context)

	# ----------------------------------------------------
	#-== @method
	# POST
	#-== Reads data from the form and creates/edits a /DocketItem object.
	# @params
	# title: the title of the item
	# category: a /DocketCategory the item belongs to
	# status: a valid /DocketStatus integer
	# priority: a valid /DocketPriority integer
	# contact: (optional) the contact info of the item
	# newupdate: (optional) text to include as an update to the item

	#-== If a /pk is provided, this will edit that item.
	# Otherwise, a new /DocketItem will be created.
	# Successful execution redirects to /DocketHome view.

	def post(self, request, pk=None, *args, **kwargs):

		item = self.get_item_and_set_context(pk)
		data, is_valid = self.validate_post_data(request)

		# set up the item and context
		item.user = self.user
		item.title = data['title']
		item.category = data['category']
		item.status = data['status']
		item.priority = data['priority']
		item.contact = data['contact']

		if data['newupdate']:
			self.context['newupdate'] = data['newupdate']

		# if it's not valid, then we can't save it
		# send the user back to the form to fix it
		if not is_valid:
			return render(request, 'docket/item.html', self.context)
		
		# update the fields on the item
		try:
			item.mark_last_update()
		except ValidationError as exc:
			self.log_error(exc, exception=exc)
		except Exception as exc:
			self.log_error('An error occurred while saving the item', exception=exc)

		if self.context['errors']:
			return render(request, 'docket/item.html', self.context)

		# create a new update, if necessary
		if data['newupdate']:
			try:
				newupdate = DocketItemUpdate.objects.create(item=item, text=data['newupdate'])
			except ValidationError as exc:
				self.log_error(exc, exception=exc)
			except Exception as exc:
				self.log_error('An error occurred while creating a new item update', exception=exc)

		if self.context['errors']:
			return render(request, 'docket/item.html', self.context)

		# mark the last_update field
		try:
			item.mark_last_update()
		except Exception as exc:
			self.log_error('An error occurred while marking the item last_update', exception=exc)

		if self.context['errors']:
			return render(request, 'docket/item.html', self.context)

		# if we haven't encountered any errors, let's go back to home
		return self.redirect('docket:home')

##########################################
#-== @class
class EditItem(CreateItem):
	#-== Form to edit or delete a user.
	# This form is derived from the /CreateItem view.
	# See that entry for more details.

	page_title = 'Edit Item'

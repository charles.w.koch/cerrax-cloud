
from enum import IntEnum
from datetime import date, timedelta

from django.utils import timezone
from django.db import models
from django.core.exceptions import ObjectDoesNotExist

from core.models import CoreModel, CerraxUser
from core.utils import Sort, Sorting, Color

#-== @h1
# Docket Models
#-== /docket.models.py
#________________________________________


COLOR_LEN = 7


##########################################
#-== @class
class DocketSorting(Sorting):
	#-== Sortings available for the /DocketSettings model.

	DATA = [
		Sort('title', 'Title'),
		Sort('category', 'Category'),
		Sort('priority', 'Priority'),
		Sort('status', 'Status'),
		Sort('created', 'Created'),
		Sort('last_update', 'Updated'),
	]

##########################################
#-== @class
class DocketPriority(IntEnum):
	#-== Enumeration for available priorities
	# @attributes
	# LOW: low priority
	# MEDIUM: medium prority
	# HIGH: high priority

	LOW = 1
	MEDIUM = 2
	HIGH = 3

	# ----------------------------------------------------
	#-== @method
	@classmethod
	def get_display(cls, value):
		#-== Returns the display text for the given priority.
		# @params
		# priority: a valid value for the /DocketPriority enum

		if value == cls.LOW:
			return 'Low'
		elif value == cls.MEDIUM:
			return 'Medium'
		elif value == cls.HIGH:
			return 'High'
		else:
			raise ValueError('Given priority does not match any valid value')


##########################################
#-== @class
class DocketStatus(IntEnum):
	#-== Enumeration for available statuses
	# @attributes
	# OPEN: item has no work started on it yet
	# WORKING: item has some progess but is not yet completed
	# ONHOLD: item has something blocking it and cannot be completed
	# CLOSED: item is completed

	OPEN = 1
	WORKING = 2
	ONHOLD = 3
	CLOSED = 4

	# ----------------------------------------------------
	#-== @method
	@classmethod
	def get_display(cls, value):
		#-== Returns the display text for the given status.
		# @params
		# priority: a valid value for the /DocketStatus enum

		if value == cls.OPEN:
			return 'Open'
		elif value == cls.WORKING:
			return 'Working'
		elif value == cls.ONHOLD:
			return 'On Hold'
		elif value == cls.CLOSED:
			return 'Closed'
		else:
			raise ValueError('Given status does not match any valid value')

##########################################
#-== @class
class DocketCategory(CoreModel):
	#-== User-defined labels which can be used to organize/filter
	# /DocketItem objects.
	
	#-== *Model Fields:*
	# @deflist
	# user: the /CerraxUser the category belongs to
	# name: a name defined by the user
	# hide_item: determines if this category should be used to hide items

	user = models.ForeignKey(CerraxUser, on_delete=models.CASCADE)
	name = models.CharField(max_length=30)
	hide_items = models.BooleanField(default=False)

	def __str__(self):
		return '{} - {}'.format(self.user, self.name)

##########################################
#-== @class
class DocketSettings(CoreModel):
	#-== User settings for the Docket app
	
	#-== *Model Fields:*
	# @deflist
	# user: the /CerraxUser the settings belong to
	# update_new_color: the hex value to color newly created items
	# update_aging_days: days after an update an item is considered "aging"
	# update_aging_color: the hex value to color "aging" items
	# update_old_days: days after an update an item is considered "old"
	# update_old_color: the hex value to color "old" items
	# update_purge_warning: days prior to purging an item is colored as a warning
	# update_aging_color: the hex value to color items that will soon be purged
	# purge_days: number of days since last update that an item is purged (deleted)
	# status_open_color: the hex value to color "open" items
	# status_working_color: the hex value to color "working" items
	# status_onhold_color: the hex value to color "on hold" items
	# status_closed_color: the hex value to color "closed" items
	# priority_low_color: the hex value to color "low" priority items
	# priority_medium_color: the hex value to color "medium" priority items
	# priority_high_color: the hex value to color "high" priority items
	# sorting: the sorting chosen to sort items in the docket
	# filter_hide_status: a string of comma-separated status names to hide in the docket
	# filter_hide_priority: a string of comma-separated priority names to hide in the docket

	user = models.ForeignKey(CerraxUser, on_delete=models.CASCADE)
	update_new_color = models.CharField(max_length=COLOR_LEN, default='#ffffff')
	update_aging_days = models.IntegerField(default=2)
	update_aging_color = models.CharField(max_length=COLOR_LEN, default='#fbe8ac')
	update_old_days = models.IntegerField(default=5)
	update_old_color = models.CharField(max_length=COLOR_LEN, default='#ff2600')
	update_purge_warning = models.IntegerField(default=10)
	update_purge_color = models.CharField(max_length=COLOR_LEN, default='#646464')
	purge_days = models.IntegerField(default=240)
	status_open_color = models.CharField(max_length=COLOR_LEN, default='#ffffff')
	status_working_color = models.CharField(max_length=COLOR_LEN, default='#cadbfa')
	status_onhold_color = models.CharField(max_length=COLOR_LEN, default='#e06664')
	status_closed_color = models.CharField(max_length=COLOR_LEN, default='#b8e1cc')
	priority_low_color = models.CharField(max_length=COLOR_LEN, default='#ffffff')
	priority_medium_color = models.CharField(max_length=COLOR_LEN, default='#fbe8ac')
	priority_high_color = models.CharField(max_length=COLOR_LEN, default='#e06664')
	sorting = models.CharField(max_length=30, choices=DocketSorting.choices(), default='priority')
	filter_hide_status = models.CharField(blank=True, max_length=20, default='')
	filter_hide_priority = models.CharField(blank=True, max_length=20, default='')

	def __str__(self):
		return '{} - settings'.format(self.user)

	# ----------------------------------------------------
	#-== @method
	def get_excluded_statuses(self):
		#-== Returns a list of statuses to exclude
		# based on the user's /filter_hide_status field.

		excludes = []
		if not self.filter_hide_status:
			return excludes
		for status in self.filter_hide_status.split(','):
			excludes.append(int(status))
		return excludes

	# ----------------------------------------------------
	#-== @method
	def get_excluded_priorities(self):
		#-== Returns a list of priorities to exclude
		# based on the user's /filter_hide_priority field.

		excludes = []
		if not self.filter_hide_priority:
			return excludes
		for priority in self.filter_hide_priority.split(','):
			excludes.append(int(priority))
		return excludes

	# ----------------------------------------------------
	#-== @method
	def get_excluded_category_ids(self):
		#-== Returns a list of category IDs to exclude
		# based on the /hide_items flag on each of the user's categories.

		excludes = []
		for cat in DocketCategory.objects.filter(user=self.user, hide_items=True):
			excludes.append(cat.id)
		return excludes

	# ----------------------------------------------------
	#-== @method
	def get_exclusions(self):
		#-== Returns a dictionary to use with Django's /exclude() function
		# to filter out unwanted /DocketItem objects.

		excludes = {
			'statuses': { 'status__in': self.get_excluded_statuses() },
			'priorities': { 'priority__in': self.get_excluded_priorities() },
			'categories': { 'category__in': self.get_excluded_category_ids() },
		}

		return excludes

	# ----------------------------------------------------
	#-== @method
	def get_priority_color(self, priority):
		#-== Returns the color set by the user for the given priority.
		# @params
		# priority: a valid value for the /DocketPriority enum

		if priority == DocketPriority.LOW:
			return self.priority_low_color
		elif priority == DocketPriority.MEDIUM:
			return self.priority_medium_color
		elif priority == DocketPriority.HIGH:
			return self.priority_high_color
		else:
			raise ValueError('Given priority does not match any valid value')

	# ----------------------------------------------------
	#-== @method
	def get_status_color(self, status):
		#-== Returns the color set by the user for the given status.
		# @params
		# status: a valid value for the /DocketStatus enum

		if status == DocketStatus.OPEN:
			return self.status_open_color
		elif status == DocketStatus.WORKING:
			return self.status_working_color
		elif status == DocketStatus.ONHOLD:
			return self.status_onhold_color
		elif status == DocketStatus.CLOSED:
			return self.status_closed_color
		else:
			raise ValueError('Given status does not match any valid value')

	# ----------------------------------------------------
	#-== @method
	def get_last_update_color(self, last_update):
		#-== Returns the color set by the user for the given date.
		# @params
		# last_update: a /date() or /datetime() object
		
		color = self.update_new_color
		if last_update is None:
			return color

		threshold = timezone.now() - timedelta(days=self.update_aging_days)
		if last_update <= threshold.date():
			color = self.update_aging_color
		threshold = timezone.now() - timedelta(days=self.update_old_days)
		if last_update <= threshold.date():
			color = self.update_old_color
		purge_warning = self.purge_days - self.update_purge_warning
		threshold = timezone.now() - timedelta(days=purge_warning)
		if last_update <= threshold.date():
			color = self.update_purge_color
		return color

##########################################
#-== @class
class DocketItem(CoreModel):
	#-== An item to display in the Docket app.
	# Typically this is some kind of task the user
	# wants to track the progress of.
	
	#-== *Model Fields:*
	# @deflist
	# user: the /CerraxUser the item belong to
	# title: the title of the item
	# category: a /DocketCategory the item belongs to
	# priority: the priority of the item (low, medium, or high)
	# status: the status of the item (open, working, on hold, or closed)
	# contact: (optional) the person to contact about this item

	user = models.ForeignKey(CerraxUser, on_delete=models.CASCADE)
	title = models.CharField(max_length=100)
	category = models.ForeignKey(DocketCategory, on_delete=models.PROTECT)
	priority = models.IntegerField(default=2)
	status = models.IntegerField(default=1)
	contact = models.CharField(max_length=50, blank=True, default='')
	last_update = models.DateField()

	def __str__(self):
		return '{} - {}'.format(self.user.username, self.title)

	# ----------------------------------------------------
	#-== @method
	def get_latest_update(self):
		#-== Returns the most recent update submitted to this item.

		return DocketItemUpdate.objects.filter(item=self).latest('updated')

	# ----------------------------------------------------
	#-== @method
	def mark_last_update(self):
		#-== Checks the most recent update of this item
		# and adjusts the /last_update field accordingly.

		latest_update = None
		try:
			latest_update = self.get_latest_update()
		except ObjectDoesNotExist:
			self.last_update = date.today()

		if latest_update:
			self.last_update = self.get_latest_update().updated.date()
		self.save()

	# ----------------------------------------------------
	#-== @method
	def set_display(self):
		#-== Adds new attributes to the object for display in a template.
		# @attributes
		# self.status_display: the display text of the item's status
		# self.status_bg_color: the background color of the status div
		# self.status_fore_color: the text color of the status div
		# self.priority_display: the display text of the item's priority
		# self.priority_bg_color: the background color of the priority div
		# self.priority_fore_color: the text color of the priority div
		# self.last_update_bg_color: the background color of the last_update div
		# self.last_update_fore_color: the text color of the last_update div
		# self.latest_update_text: the text from the most recent update

		settings = DocketSettings.objects.get(user=self.user)

		self.status_display = DocketStatus.get_display(self.status)
		self.status_bg_color = settings.get_status_color(self.status)
		self.status_fore_color = Color.fore_color(self.status_bg_color)

		self.priority_display = DocketPriority.get_display(self.priority)
		self.priority_bg_color = settings.get_priority_color(self.priority)
		self.priority_fore_color = Color.fore_color(self.priority_bg_color)

		self.last_update_bg_color = settings.get_last_update_color(self.last_update)
		self.last_update_fore_color = Color.fore_color(self.last_update_bg_color)

		try: 
			self.latest_update_text = self.get_latest_update().text
		except:
			self.latest_update_text = ''


##########################################
#-== @class
class DocketItemUpdate(CoreModel):
	#-== An update to a /DocketItem .

	#-== *Model Fields:*
	# @deflist
	# item: the /DocketItem the update belongs to
	# text: the text to put in the update (maximum 250 characters)

	item = models.ForeignKey(DocketItem, on_delete=models.CASCADE)
	text = models.CharField(max_length=250)

	def __str__(self):
		return '{} - {}'.format(self.item, self.created)


var statdrop;
var pridrop;

window.onload = (event) => {
	statdrop = document.getElementById('statusDropdown')
	statdrop.addEventListener("change", setStatusColors);

	pridrop = document.getElementById('priorityDropdown')
	pridrop.addEventListener("change", setPriorityColors);
}

function setStatusColors(event) {
	let statusId = statdrop.value
	let stat = statuses[statusId]
	statdrop.style.color = stat.fore_color
	statdrop.style['background-color'] = stat.bg_color
}

function setPriorityColors(event) {
	let priorityId = pridrop.value
	let pri = priorities[priorityId]
	pridrop.style.color = pri.fore_color
	pridrop.style['background-color'] = pri.bg_color
}

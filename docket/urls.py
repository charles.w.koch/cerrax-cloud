from django.urls import path, include

from docket import views


urlpatterns = [
	path('', views.DocketHome.as_view(), name = 'home'),
    path('settings/', views.DocketSettingsPage.as_view(), name = 'settings'),
    path('sort/', views.SortPage.as_view(), name = 'sort'),
    path('filter/', views.FilterPage.as_view(), name = 'filter'),
    path('item/', views.CreateItem.as_view(), name = 'create-item'),
    path('item/<int:pk>/', views.EditItem.as_view(), name = 'edit-item'),
]
var fileLines;		// holds the entire file contents
var displayLines;	// holds only the lines to be displayed

var fileLoader;		// loads the file
var fileContentsArea; // the area where results are displayed

var presetDropdown;

var regex_box_area;  // the area for the regexes
var regexes;
var regex_num;

document.onkeypress = function(e)
{
	// filter results whenever the Enter key is pressed
	var x = e || window.event;
	var key = (x.keyCode || x.which);
	if(key == 13 || key == 3)
	{
		filterAndDisplay();
	}
}

window.onload = function()
{
	// initiate the file loader and results area
	fileLoader = document.getElementById('file_picker');
	fileContentsArea = document.getElementById('file_contents_area');
	// initiate the regex area
	regex_box_area = document.getElementById('regex_box_area');
	regexes = [];
	regex_num = 0;
	// initiate the preset dropdwon menu
	presetDropdown = document.getElementById('preset_list');

	// initialize the data structures
	fileLines = [];
	displayLines = [];

	// set the method to load a new file whenever it is selected
	fileLoader.addEventListener('change', loadFile);
	loadPresets();
	presetDropdown.addEventListener('change', changeToPreset);
}

function loadPresets()
{
	var opt = document.createElement('option');
	opt.setAttribute('value', -1);
	opt.setAttribute('selected', true);
	presetDropdown.appendChild(opt);

	for(i in presets)
	{
		opt = document.createElement('option');
		opt.setAttribute('value', i);
		opt.innerHTML = presets[i].name;
		presetDropdown.appendChild(opt);
	}
}

function changeToPreset()
{
	var name;
	var s;
	var p_ind = presetDropdown.value;
	if (p_ind < 0)
	{
		regex_box_area.innerHTML ="";
		regexes = [];
		return;
	}
	for(i in presets[p_ind].regex_boxes)
	{
		name = presets[p_ind].regex_boxes[i].name;
		s = presets[p_ind].regex_boxes[i].settings;
		addRegex(name, s.regex, s.color, s.case_sense);
	}
}

function loadFile()
{
	var file = fileLoader.files[0];
	var reader = new FileReader();
	reader.onloadend = function(){readFile(reader.result)}
	console.log("Reading file...");
	reader.readAsText(file);
}

function readFile(incoming)
{
	console.log("Parsing file...");
	fileLines = incoming.split("\n");

	filterAndDisplay();
}

function addRegex(name, regex, color, case_bool)
{
	// current number of children + 1
	var index = regex_num + 1;
	regex_num = index;

	// create a new regex_box
	var box = document.createElement('div');
	box.setAttribute('class', 'regex_box');
	box.setAttribute('id', 'regex_box_' + index.toString());

	// create a tool box
	var tool_box = document.createElement('div');
	tool_box.setAttribute('class', 'name_field');
	// create the tool (name field)
	tool_box.innerHTML = name;
	// append the tool and the tool box
	box.appendChild(tool_box);

	// create a tool box
	var tool_box = document.createElement('div');
	tool_box.setAttribute('class', 'regex_field');
	// create the tool (regex field)
	var tool = document.createElement('input');
	tool.setAttribute('type', 'text');
	tool.setAttribute('size', 50);
	tool.setAttribute('id', 'regex_' + index.toString());
	tool.setAttribute('value', regex);
	// append the tool and the tool box
	tool_box.appendChild(tool);
	box.appendChild(tool_box);

	// create the tool box
	tool_box = document.createElement('div');
	tool_box.setAttribute('class', 'tool_box');
	// create the tool (color picker)
	tool = document.createElement('input');
	tool.setAttribute('type', 'color');
	tool.setAttribute('id', 'color_' + index.toString());
	tool.value = color;
	// append the tool and the tool box
	tool_box.appendChild(tool);
	box.appendChild(tool_box);

	// create the tool box
	tool_box = document.createElement('div');
	tool_box.setAttribute('class', 'tool_box');
	// create the tool (case sensitive checkbox)
	tool = document.createElement('input');
	tool.setAttribute('type', 'checkbox');
	tool.setAttribute('id', 'case_bool_' + index.toString());
	if (case_bool == 'true')
	{
		tool.setAttribute('checked', true);
	}
	// append the tool and the tool box
	tool_box.appendChild(tool);
	// create a text node (checkbox label)
	text_node = document.createTextNode('Case Sensitive');
	tool_box.appendChild(text_node);
	box.appendChild(tool_box);

	// create the tool box
	tool_box = document.createElement('div');
	tool_box.setAttribute('class', 'tool_box');
	// create the tool (delete button)
	tool = document.createElement('input');
	tool.setAttribute('type', 'submit');
	tool.setAttribute('value', 'Delete');
	tool.setAttribute('onclick', 'deleteRegex(' + index.toString() + ')');
	tool.setAttribute('id', 'delete_' + index.toString());
	// append the tool and the tool box
	tool_box.appendChild(tool);
	box.appendChild(tool_box);

	regex_box_area.appendChild(box);
	setRegex(box);

}

function deleteRegex(index)
{
	console.log(index)
	var box = document.getElementById('regex_box_' + index.toString());
	regex_box_area.removeChild(box);
	regexes.splice(regexes.indexOf(box), 1);
}

function setRegex(child)
{
	rgexp = child.children[1].children[0].value;
	color = child.children[2].children[0].value;
	case_sensitive = child.children[3].children[0].checked;
	regexes.push(child);
}

function checkRegexes(line, line_number)
{
	var regexp = /.*/;

	for(i in regexes)
	{
		var child = regexes[i];
		var rgexp = child.children[1].children[0].value;
		var color = child.children[2].children[0].value;
		var case_sensitive = child.children[3].children[0].checked;
		if(rgexp == "")
		{
			return null;
		}
		else
		{
			if(case_sensitive)
			{
				regexp = new RegExp(rgexp);
			}
			else
			{
				regexp = new RegExp(rgexp, 'i');
			}
			var found_regex = regexp.exec(line);
			if(found_regex !== null)
			{
				return {num:(parseInt(line_number)+1), text:line, ind:found_regex.index, color:color};
			}
		}
	}
	return null;
}

function filterAndDisplay()
{

	console.log("Beginning search...")
	// get the regexes
	if (regexes.length <= 0)
	{
		console.log("No regexes to filter by!");
		return;
	}

	// initialize display data
	displayLines = [];

	// initalize display counter
	var o = 0;

	var new_line;

	// filter results
	if(fileLines.length <= 0)
	{
		console.log("No file to search!");
	}

	for (i in fileLines)
	{
		new_line = checkRegexes(fileLines[i], i);
		if (new_line !== null)
		{
			//console.log(new_line)
			displayLines[o] = new_line;
			o++
		}
	}

	// clear display area
	fileContentsArea.innerHTML = "";

	if(displayLines.length <= 0)
	{
		var whole_line = document.createElement("div");
		whole_line.setAttribute('class', 'italic_line');
		whole_line.innerHTML = "There are no results to display.";
		fileContentsArea.appendChild(whole_line);
	}

	console.log("Rendering HTML...");

	// display results
	for (i in displayLines)
	{
		// create line container
		var whole_line = document.createElement("div");
		whole_line.setAttribute('class', 'whole_line');
		whole_line.setAttribute('style', 'background-color:' + displayLines[i].color + ';');
		// create line number display
		var linediv = document.createElement("div");
		linediv.setAttribute('class', 'line_number');
		linediv.innerHTML = displayLines[i].num;
		whole_line.appendChild(linediv);
		// create text display
		linediv = document.createElement("div");
		linediv.setAttribute('class', 'text_line');
		var str_out = displayLines[i].text;
		str_out = str_out.replace('<', '&lt;');
		str_out = str_out.replace('>', '&gt;');
		linediv.innerHTML = str_out;
		// add highlighting
		// var stylediv = document.createElement("span");
		// // removed highlighting for now, may put back in later
		// //stylediv.setAttribute('style', 'background-color:yellow;');
		// str_out = displayLines[i].text.substr(displayLines[i].ind, regex.length);
		// str_out = str_out.replace('<', '&lt;');
		// str_out = str_out.replace('>', '&gt;');
		// stylediv.innerHTML = str_out;
		// linediv.appendChild(stylediv);
		// // add rest of the text
		// str_out = displayLines[i].text.slice(displayLines[i].ind + regex.length);
		// str_out = str_out.replace('<', '&lt;');
		// str_out = str_out.replace('>', '&gt;');
		// linediv.innerHTML += str_out;
		whole_line.appendChild(linediv);
		fileContentsArea.appendChild(whole_line);
	}

	console.log("Filter and Display complete.");

}


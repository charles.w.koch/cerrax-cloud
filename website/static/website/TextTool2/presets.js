var presets = []


// var presets = [
// {
// 	name:'Talkman dialogs, operator, and LUTs',
// 	regex_boxes: [
// 	{
// 		name:'Talkman',
// 		settings: {
// 			regex:'##',
// 			color:'#99FF99',
// 			case_sense:'true'
// 		}
// 	},
// 	{
// 		name:'Operator',
// 		settings: {
// 			regex:'\\^{2}\\w',
// 			color:'#99CCFF',
// 			case_sense:'true'
// 		}
// 	},
// 	{
// 		name:'LUTs/ODRs',
// 		settings: {
// 			regex:'Data\\s(Sent|Received)',
// 			color:'#FFCCCC',
// 			case_sense:'true'
// 		}
// 	}
// 	]
// },
// {
// 	name:'Talkman events',
// 	regex_boxes: [
// 	{
// 		name:'Button Event',
// 		settings: {
// 			regex:'EVENT\\s--',
// 			color:'#FFF18D',
// 			case_sense:'true'
// 		}
// 	},
// 	{
// 		name:'Other Events',
// 		settings: {
// 			regex:'EVENT:',
// 			color:'#FFF18D',
// 			case_sense:'true'
// 		}
// 	}
// 	]
// },
// {
// 	name:'Talkman wireless info',
// 	regex_boxes: [
// 	{
// 		name:'Signal Strength',
// 		settings: {
// 			regex:'Signal\\sStrength',
// 			color:'#C6C6C6',
// 			case_sense:'true'
// 		}
// 	},
// 	{
// 		name:'Access Points',
// 		settings: {
// 			regex:'Access\\sPoint',
// 			color:'#C6C6C6',
// 			case_sense:'true'
// 		}
// 	},
// 	{
// 		name:'Access Point info',
// 		settings: {
// 			regex:'AP\\sMON:',
// 			color:'#C6C6C6',
// 			case_sense:'true'
// 		}
// 	}
// 	]
// },
// {
// 	name:'Talkman scanner info',
// 	regex_boxes: [
// 	{
// 		name:'Scanner Activation',
// 		settings: {
// 			regex:'Voc\\sImgCallback',
// 			color:'#CF9AFF',
// 			case_sense:'true'
// 		}
// 	},
// 	{
// 		name:'Label info',
// 		settings: {
// 			regex:'\\sLabel\\s|Bytes\\s',
// 			color:'#CF9AFF',
// 			case_sense:'true'
// 		}
// 	}
// 	]
// }
// ];
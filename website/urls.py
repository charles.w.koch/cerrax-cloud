from django.urls import path, include

from website import views

urlpatterns = [
    path('', views.MainWebsiteView.as_view(), name = 'index'),
    path('docs/<slug:page>', views.DocsView.as_view(), name = 'docs'),
    path('TextTool2', views.TextTool2.as_view() , name = 'texttool2'),
    path('gizeel', views.Gizeel.as_view() , name = 'gizeel'),
]